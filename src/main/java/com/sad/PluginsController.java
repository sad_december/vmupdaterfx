package com.sad;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.sad.utils.InfoFromTables;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class PluginsController implements Initializable {

    @FXML
    public JFXCheckBox PublicationsOfflineClient;

    @FXML
    public JFXCheckBox BackupSpot;

    @FXML
    public JFXCheckBox cqms_risks_server;

    @FXML
    public JFXCheckBox Actualization;

    @FXML
    public JFXCheckBox archive_docs_server;

    @FXML
    public JFXCheckBox BookTabs;

    @FXML
    public JFXCheckBox CatalogueExport;

    @FXML
    public JFXCheckBox Comments;

    @FXML
    public JFXCheckBox ConsideredCopies;

    @FXML
    public JFXCheckBox cqms_audits_server;

    @FXML
    public JFXCheckBox cqms_fixation_base_crm_server;

    @FXML
    public JFXCheckBox cqms_fixation_base_server;

    @FXML
    public JFXCheckBox cqms_monitoring_server;

    @FXML
    public JFXCheckBox custom_considered_copies_report_server;

    @FXML
    public JFXCheckBox custom_nd_change_report_server;

    @FXML
    public JFXCheckBox custom_subscriptions_report_server;

    @FXML
    public JFXCheckBox DBMigration;

    @FXML
    public JFXCheckBox DIPImport;

    @FXML
    public JFXCheckBox DocumentReplacer;

    @FXML
    public JFXCheckBox document_replacer_server;

    @FXML
    public JFXCheckBox Duplicates;

    @FXML
    public JFXCheckBox EbsConfiguration;

    @FXML
    public JFXCheckBox expiring_report_server;

    @FXML
    public JFXCheckBox FastInput;

    @FXML
    public JFXCheckBox Favorites;

    @FXML
    public JFXCheckBox FIASImport;

    @FXML
    public JFXCheckBox fill_report_server;

    @FXML
    public JFXCheckBox FullPreviewFiles;

    @FXML
    public JFXCheckBox fund_report_server;

    @FXML
    public JFXCheckBox kbntind_extension_server;

    @FXML
    public JFXCheckBox linked_report_server;

    @FXML
    public JFXCheckBox nd_changes_server;

    @FXML
    public JFXCheckBox ND_subscribe;

    @FXML
    public JFXCheckBox NormativeControl;

    @FXML
    public JFXCheckBox NormdocsConverters;

    @FXML
    public JFXCheckBox omk_extension_server;

    @FXML
    public JFXCheckBox omk_wss_server;

    @FXML
    public JFXCheckBox PublicationsOffline;

    @FXML
    public JFXCheckBox PublicFileAccess;

    @FXML
    public JFXCheckBox RatingPlugin;

    @FXML
    public JFXCheckBox registration_server;

    @FXML
    public JFXCheckBox reports_server;

    @FXML
    public JFXCheckBox Reviews;

    @FXML
    public JFXCheckBox RSSPlugin;

    @FXML
    public JFXCheckBox SavedSearch;

    @FXML
    public JFXCheckBox Selections;

    @FXML
    public JFXCheckBox stubfile_server;

    @FXML
    public JFXCheckBox synchronization_server;

    @FXML
    public JFXCheckBox TestSystem;

    @FXML
    public JFXCheckBox usage_report_server;

    @FXML
    public JFXCheckBox UserData;

    @FXML
    public JFXCheckBox ViewerCopies;

    @FXML
    public JFXCheckBox ViewerLink;

    @FXML
    public JFXCheckBox Voting;

    @FXML
    public JFXCheckBox vtb24plugin_server;

    @FXML
    public JFXCheckBox xml_export_server;

    @FXML
    public JFXCheckBox ActualizationClient;

    @FXML
    public JFXCheckBox archive_docs_client;

    @FXML
    public JFXCheckBox av_excel_import_client;

    @FXML
    public JFXCheckBox BackupSpotClient;

    @FXML
    public JFXCheckBox BookTabsClient;

    @FXML
    public JFXCheckBox CatalogueExportClient;

    @FXML
    public JFXCheckBox ConsideredCopiesClient;

    @FXML
    public JFXCheckBox DIPImportClient;

    @FXML
    public JFXCheckBox DuplicatesClient;

    @FXML
    public JFXCheckBox FastInputClient;

    @FXML
    public JFXCheckBox FoldersHierarchy;

    @FXML
    public JFXCheckBox HelpPlugin;

    @FXML
    public JFXCheckBox HelpPluginClient;

    @FXML
    public JFXCheckBox MassInheritance;

    @FXML
    public JFXCheckBox MGOK;

    @FXML
    public JFXCheckBox ND_subscribeClient;

    @FXML
    public JFXCheckBox NormativeControlClient;

    @FXML
    public JFXCheckBox NormdocsConvertersClient;

    @FXML
    public JFXCheckBox RatingPluginClient;

    @FXML
    public JFXCheckBox registration_client;

    @FXML
    public JFXCheckBox RSSPluginClient;

    @FXML
    public JFXCheckBox SavedSearchClient;

    @FXML
    public JFXCheckBox SelectionsClient;

    @FXML
    public JFXCheckBox stubfile_client;

    @FXML
    public JFXCheckBox TestSystemClient;

    @FXML
    public JFXCheckBox vtb24plugin_client;

    @FXML
    public JFXButton acceptPlugins;

    @FXML
    public JFXCheckBox considered_copies_workflow_server;

    @FXML
    public JFXCheckBox considered_copies_workflow_client;

    @FXML
    public JFXCheckBox file_binder_server;

    @FXML
    public JFXCheckBox WorkflowClientV1;

    @FXML
    public JFXCheckBox WorkflowV1;

    @FXML
    public JFXCheckBox WorkflowV2;

    @FXML
    public JFXCheckBox WorkflowClientV2;

    @FXML
    public JFXCheckBox file_binder_client;

    @FXML
    public JFXCheckBox onlyoffice_server;

    private StringBuilder client = new StringBuilder("");
    private StringBuilder server = new StringBuilder("");
    private boolean sync = false;


    @FXML
    public void acceptPlugins(ActionEvent event) throws IOException {
        grabinfo();

        FXApp.infoFromTables = new InfoFromTables();
        FXApp.infoFromTables.clientPlugins = client.toString();
        FXApp.infoFromTables.serverPlugins = server.toString();
        FXApp.infoFromTables.synchroProxy = sync;

        Stage stage = (Stage) WorkflowClientV1.getScene().getWindow();
        FXApp.infoFromTables.asTempateName = stage.getTitle();
        stage.close();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        server = new StringBuilder("");
        client = new StringBuilder("");
    }

    private void grabinfo(){
        if (Actualization.isSelected())
            server.append("Actualization.jar\n");
        if (archive_docs_server.isSelected())
            server.append("archive-docs-server.jar\n");
        if (BookTabs.isSelected())
            server.append("BookTabs.jar\n");
        if (CatalogueExport.isSelected())
            server.append("CatalogueExport.jar\n");
        if (Comments.isSelected())
            server.append("Comments.jar\n");
        if (ConsideredCopies.isSelected())
            server.append("ConsideredCopies.jar\n");
        if (cqms_audits_server.isSelected())
            server.append("cqms-audits-server.jar\n");
        if (cqms_fixation_base_crm_server.isSelected())
            server.append("cqms-fixation-base-crm-server.jar\n");
        if (cqms_fixation_base_server.isSelected())
            server.append("cqms-fixation-base-server.jar\n");
        if (cqms_monitoring_server.isSelected())
            server.append("cqms-monitoring-server.jar\n");
        if (custom_considered_copies_report_server.isSelected())
            server.append("custom-considered-copies-report-server.jar\n");
        if (custom_nd_change_report_server.isSelected())
            server.append("custom-nd-change-report-server.jar\n");
        if (custom_subscriptions_report_server.isSelected())
            server.append("custom-subscriptions-report-server.jar\n");
        if (DBMigration.isSelected())
            server.append("DBMigration.jar\n");
        if (DIPImport.isSelected())
            server.append("DIPImport.jar\n");
        if (DocumentReplacer.isSelected())
            server.append("DocumentReplacer.jar\n");
        if (document_replacer_server.isSelected())
            server.append("document-replacer-server.jar\n");
        if (Duplicates.isSelected())
            server.append("Duplicates.jar\n");
        if (EbsConfiguration.isSelected())
            server.append("EbsConfiguration.jar\n");
        if (expiring_report_server.isSelected())
            server.append("expiring-report-server.jar\n");
        if (FastInput.isSelected())
            server.append("FastInput.jar\n");
        if (Favorites.isSelected())
            server.append("Favorites.jar\n");
        if (FIASImport.isSelected())
            server.append("FIASImport.jar\n");
        if (fill_report_server.isSelected())
            server.append("fill-report-server.jar\n");
        if (file_binder_server.isSelected())
            server.append("file-binder-server.jar\n");
        if (FullPreviewFiles.isSelected())
            server.append("FullPreviewFiles.jar\n");
        if (fund_report_server.isSelected())
            server.append("fund-report-server.jar\n");
        if (HelpPlugin.isSelected())
            server.append("HelpPlugin.jar\n");
        if (kbntind_extension_server.isSelected())
            server.append("kbntind-extension-server.jar\n");
        if (linked_report_server.isSelected())
            server.append("linked-report-server.jar\n");
        if (nd_changes_server.isSelected())
            server.append("nd-changes-server.jar\n");
        if (ND_subscribe.isSelected())
            server.append("ND-subscribe.jar\n");
        if (NormativeControl.isSelected())
            server.append("NormativeControl.jar\n");
        if (NormdocsConverters.isSelected())
            server.append("NormdocsConverters.jar\n");
        if (omk_extension_server.isSelected())
            server.append("omk-extension-server.jar\n");
        if (omk_wss_server.isSelected())
            server.append("omk-wss-server.jar\n");
        if (onlyoffice_server.isSelected())
            server.append("onlyoffice-server.jar\n");
        if (PublicationsOffline.isSelected())
            server.append("PublicationsOffline.jar\n");
        if (PublicFileAccess.isSelected())
            server.append("PublicFileAccess.jar\n");
        if (RatingPlugin.isSelected())
            server.append("RatingPlugin.jar\n");
        if (registration_server.isSelected())
            server.append("registration-server.jar\n");
        if (reports_server.isSelected())
            server.append("reports-server.jar\n");
        if (Reviews.isSelected())
            server.append("Reviews.jar\n");
        if (RSSPlugin.isSelected())
            server.append("RSSPlugin.jar\n");
        if (SavedSearch.isSelected())
            server.append("SavedSearch.jar\n");
        if (Selections.isSelected())
            server.append("Selections.jar\n");
        if (stubfile_client.isSelected())
            server.append("stubfile-server.jar\n");
        if (synchronization_server.isSelected()) {
            sync = true;
            server.append("synchronization-server.jar\n");
        }
        if (TestSystem.isSelected())
            server.append("TestSystem.jar\n");
        if (usage_report_server.isSelected())
            server.append("usage-report-server.jar\n");
        if (UserData.isSelected())
            server.append("UserData.jar\n");
        if (ViewerCopies.isSelected())
            server.append("ViewerCopies.jar\n");
        if (ViewerLink.isSelected())
            server.append("ViewerLink.jar\n");
        if (Voting.isSelected())
            server.append("Voting.jar\n");
        if (vtb24plugin_server.isSelected())
            server.append("vtb24plugin-server.jar\n");
        if (WorkflowV1.isSelected())
            server.append("workflow-v1.jar\n");
        if (WorkflowV2.isSelected())
            server.append("workflow-v2.jar\n");
        if (xml_export_server.isSelected())
            server.append("xml-export-server.jar\n");
        if (cqms_risks_server.isSelected())
            server.append("cqms-risks-server.jar\n");
        if (BackupSpot.isSelected())
            server.append("BackupSpot.jar\n");
        if (considered_copies_workflow_server.isSelected())
            server.append("custom-considered-copies-report-server.jar\n");
        if (file_binder_server.isSelected())
            server.append("file_binder_server.jar\n");

        if (BookTabsClient.isSelected())
            client.append("BookTabs.jar\n");
        if (ActualizationClient.isSelected())
            client.append("Actualization.jar\n");
        if (archive_docs_client.isSelected())
            client.append("archive-docs-client.jar\n");
        if (av_excel_import_client.isSelected())
            client.append("av-excel-import-client.jar\n");
        if (BackupSpotClient.isSelected())
            client.append("BackupSpot.jar\n");
        if (CatalogueExportClient.isSelected())
            client.append("CatalogueExport.jar\n");
        if (ConsideredCopiesClient.isSelected())
            client.append("ConsideredCopies.jar\n");
        if (DIPImportClient.isSelected())
            client.append("DIPImport.jar\n");
        if (DuplicatesClient.isSelected())
            client.append("Duplicates.jar\n");
        if (FastInputClient.isSelected())
            client.append("FastInput.jar\n");
        if (FoldersHierarchy.isSelected())
            client.append("FoldersHierarchy.jar\n");
        if (HelpPluginClient.isSelected())
            client.append("HelpPlugin.jar\n");
        if (MassInheritance.isSelected())
            client.append("MassInheritance.jar\n");
        if (MGOK.isSelected())
            client.append("MGOK.jar\n");
        if (file_binder_client.isSelected())
            client.append("file-binder-client.jar\n");
        if (ND_subscribeClient.isSelected())
            client.append("ND-subscribe.jar\n");
        if (NormativeControlClient.isSelected())
            client.append("NormativeControl.jar\n");
        if (NormdocsConvertersClient.isSelected())
            client.append("NormdocsConverters.jar\n");
        if (PublicationsOfflineClient.isSelected())
            client.append("PublicationsOffline.jar\n");
        if (RatingPluginClient.isSelected())
            client.append("RatingPlugin.jar\n");
        if (registration_client.isSelected())
            client.append("registration-client.jar\n");
        if (RSSPluginClient.isSelected())
            client.append("RSSPlugin.jar\n");
        if (SavedSearchClient.isSelected())
            client.append("SavedSearch.jar\n");
        if (SelectionsClient.isSelected())
            client.append("Selections.jar\n");
        if (stubfile_client.isSelected())
            client.append("stubfile-client.jar\n");
        if (TestSystemClient.isSelected())
            client.append("TestSystem.jar\n");
        if (vtb24plugin_client.isSelected())
            client.append("vtb24plugin-client.jar\n");
        if (WorkflowClientV1.isSelected())
            client.append("workflow-v1.jar\n");
        if (WorkflowClientV2.isSelected())
            client.append("workflow-v2.jar\n");
        if (considered_copies_workflow_client.isSelected())
            client.append("considered-copies-workflow-client.jar\n");
    }
}