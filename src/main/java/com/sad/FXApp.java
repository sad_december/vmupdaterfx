package com.sad;

import com.sad.Wokers.ASWorker;
import com.sad.utils.InfoFromTables;
import com.sad.utils.fileUtil;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class FXApp extends Application {

    private static final Logger logger = LogManager.getLogger(ASWorker.class);

    static InfoFromTables infoFromTables;
    static String puttyPath;

    public static void main(String[] args) throws Exception {
        try {
            puttyPath = ExportResource();
        } catch (Exception ignored) {
        }
        infoFromTables = new InfoFromTables();
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        infoFromTables = new InfoFromTables();
        AnchorPane layout = FXMLLoader.load(getClass().getResource("/fxmls/MainScene.fxml"));
        Scene scene = new Scene(layout);
        stage.setScene(scene);
        stage.setResizable(false);

        Platform.setImplicitExit(false);

        stage.show();
        stage.toFront();

        stage.setTitle("lazy vm updater");

        stage.setOnCloseRequest(e -> {
            try {
                new fileUtil().deleteDirectory(new File(System.getProperty("user.home") + "\\vmUpdater\\"));
            } catch (Exception ignored) {
            }
            Platform.exit();
            System.exit(0);
        });
    }

    private static String ExportResource() {
        InputStream stream = null;
        OutputStream resStreamOut = null;
        String jarFolder = "";
        try {
            stream = FXApp.class.getResourceAsStream("/PUTTY.EXE");
            if (stream == null) {
                throw new Exception("Cannot get resource \"" + "/PUTTY.EXE" + "\" from Jar file.");
            }
            int readBytes;
            byte[] buffer = new byte[4096];
            jarFolder = new File(FXApp.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParentFile().getPath().replace('\\', '/');
            resStreamOut = new FileOutputStream(jarFolder + "/PUTTY.EXE");
            while ((readBytes = stream.read(buffer)) > 0) {
                resStreamOut.write(buffer, 0, readBytes);
            }
        } catch (Exception ex) {
            logger.error(ex);
        } finally {
            try {
                assert stream != null;
                stream.close();
                assert resStreamOut != null;
                resStreamOut.close();
            } catch (IOException e) {
                logger.error(e);
            }
        }
        return jarFolder + "/PUTTY.EXE";
    }
}