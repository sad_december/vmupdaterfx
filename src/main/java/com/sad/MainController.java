package com.sad;

import com.jfoenix.controls.*;
import com.sad.Wokers.ASWorker;
import com.sad.Wokers.VMControlWorker;
import com.sad.Wokers.WebWorker;
import com.sad.putty.OnlineChecker;
import com.sad.putty.PuttyWorker;
import com.sad.utils.InfoFromTables;
import com.sad.utils.LocalUtils;
import com.sad.utils.ShittySettings;
import com.sad.utils.fileUtil;
import com.vdurmont.emoji.EmojiParser;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.DragEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MainController implements Initializable {

    private static final Logger logger = LogManager.getLogger(MainController.class);

    @FXML
    public JFXButton deployASButton;

    @FXML
    public AnchorPane rootPane;

    @FXML
    public StackPane rootStackPane;

    @FXML
    public JFXComboBox<String> infoHostBox;

    @FXML
    public JFXButton DatabseButton;

    @FXML
    public JFXButton GFAppsButton;

    @FXML
    public JFXButton startGFButton;

    @FXML
    public JFXButton stopGFButton;

    @FXML
    public JFXButton restartGFButton;

    @FXML
    public JFXButton puttyCmdButton;

    @FXML
    public JFXButton puttyCmdAllOnline;

    @FXML
    public JFXTextField stringInLog;

    @FXML
    public JFXButton puttyCmdLogButton;

    @FXML
    public JFXButton killJavaButton;

    @FXML
    public JFXDatePicker dateSetter;

    @FXML
    public JFXButton setDateBUtton;

    @FXML
    public JFXButton allInfoButton;

    @FXML
    public JFXButton enableGFButton;

    @FXML
    public JFXButton disableGFButton;

    @FXML
    public JFXButton ReloadGFButton;

    @FXML
    public JFXTimePicker timeSetter;

    @FXML
    public Tab archiveTab;

    @FXML
    public Label labelForFileASDeploy;

    @FXML
    public Tab webTab;

    @FXML
    public Label labelForFileWebDeploy;

    @FXML
    public JFXButton openASFileButton;

    @FXML
    public JFXButton refreshASFileButton;

    @FXML
    public JFXButton openWebFileButton;

    @FXML
    public JFXButton refreshWebFileButton;

    @FXML
    public ToggleGroup aSwebGroup;

    @FXML
    public Tab massImportTab;

    @FXML
    public JFXRadioButton testvmRadioButton;

    @FXML
    public JFXRadioButton testvm3RadioButton;

    @FXML
    public JFXRadioButton testvm6RadioButton;

    @FXML
    public JFXRadioButton testvm7RadioButton;

    @FXML
    public JFXRadioButton testvm10RadioButton;

    @FXML
    public JFXRadioButton testvm11RadioButton;

    @FXML
    public JFXRadioButton testvm12RadioButton;

    @FXML
    public JFXRadioButton testvm13RadioButton;

    @FXML
    public JFXRadioButton testvm14RadioButton;

    @FXML
    public JFXTextField testvmConfigure;

    @FXML
    public JFXTextField testvm3Configure;

    @FXML
    public JFXComboBox<String> testvm6Configure;

    @FXML
    public JFXTextField testvm10Configure;

    @FXML
    public JFXTextField testvm11Configure;

    @FXML
    public JFXComboBox<String> testvm12Configure;

    @FXML
    public JFXTextField testvm13Configure;

    @FXML
    public JFXTextField testvm14Configure;

    @FXML
    public JFXButton buttonFileToMassDeploy;

    @FXML
    public JFXTextField fileToMassDeploy;

    @FXML
    public JFXToggleButton asToggle;

    @FXML
    public JFXToggleButton webToggle;

    @FXML
    public JFXButton refreshMDFileButton;

    @FXML
    private JFXComboBox<String> ASDeployHostBox;

    @FXML
    private JFXButton Default;

    @FXML
    private JFXButton Ebs;

    @FXML
    private JFXButton Mchs;

    @FXML
    private JFXButton Helicopters;

    @FXML
    private JFXButton OMK;

    @FXML
    private JFXButton CQMS;

    @FXML
    private JFXButton KBNTI;

    @FXML
    private JFXButton All;

    @FXML
    private JFXButton Clear;

    @FXML
    private JFXButton YourOwnPlugins;

    @FXML
    private JFXButton DefaultPlugins;

    @FXML
    private ToggleGroup webTemplageGroup;

    @FXML
    private JFXRadioButton yourOwnTemplate;

    @FXML
    private ToggleGroup gfAppGroup;

    @FXML
    private JFXRadioButton yourOwnGFApp;

    @FXML
    private JFXTextField yourWebTempalte;

    @FXML
    private JFXTextField yourGFApp;

    @FXML
    private JFXComboBox<String> WebDeployHostBox;

    @FXML
    private JFXButton webDeployButton;

    @FXML
    private JFXButton gfInfoButton;

    private ExecutorService executor;

    private PluginsController pluginsController;

    private String lastAstemplate;

    private File asFileToDeploy;

    private boolean asFileChosen;

    private File webFileToDeploy;

    private File toMassDeploy;

    private boolean webFileChosen;

    private boolean mdFileChosen;

    private final FileChooser fileChooser = new FileChooser();

    private ShittySettings shittySettings = new ShittySettings();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        webFileChosen = false;
        asFileChosen = false;
        mdFileChosen = false;
        refreshMDFileButton.setText(EmojiParser.parseToUnicode(":arrows_counterclockwise:"));
        refreshASFileButton.setText(EmojiParser.parseToUnicode(":arrows_counterclockwise:"));
        refreshWebFileButton.setText(EmojiParser.parseToUnicode(":arrows_counterclockwise:"));
        LocalUtils localUtils = new LocalUtils();
        executor = Executors.newFixedThreadPool(localUtils.getHosts().size());
        stringInLog.setText("1000");
        dateSetter.setValue(LocalDate.now());
        timeSetter.set24HourView(true);
        timeSetter.setValue(LocalTime.now());
        gfInfoButton.setText(EmojiParser.parseToUnicode(":information_source:"));
        WebDeployHostBox.setItems(FXCollections.observableList(localUtils.getHosts()));
        ASDeployHostBox.setItems(FXCollections.observableList(localUtils.getHosts()));
        infoHostBox.setItems(FXCollections.observableList(localUtils.getHosts()));
        WebDeployHostBox.getSelectionModel().select(0);
        ASDeployHostBox.getSelectionModel().select(0);
        infoHostBox.getSelectionModel().select(0);
        Platform.setImplicitExit(false);
    }

    @FXML
    public void executeWebDeploy(ActionEvent event) {
        Platform.runLater(() -> {
            FXApp.infoFromTables = new InfoFromTables();
            FXApp.infoFromTables.hostname = WebDeployHostBox.getSelectionModel().getSelectedItem();
            FXApp.infoFromTables.webTemplateName = getWebTemplate();
            FXApp.infoFromTables.glassfishAppName = getCheckedGFApp();

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Web Deploy Confirmation");
            alert.setHeaderText("Are you sure want to deploy " + FXApp.infoFromTables.webTemplateName + " to " + FXApp.infoFromTables.hostname);
            alert.setContentText(new LocalUtils().getTextInfoAboutHost(FXApp.infoFromTables.hostname));

            Optional<ButtonType> option = alert.showAndWait();
            if (option.get() == ButtonType.OK) {
                logger.info(FXApp.infoFromTables.hostname + " " + FXApp.infoFromTables.webTemplateName + " " + FXApp.infoFromTables.glassfishAppName);
                WebWorker webWorker = new WebWorker(FXApp.infoFromTables);
                if (webFileChosen) {
                    webWorker.setFileToDeploy(webFileToDeploy);
                }
                webWorker.start();
            }
        });
    }

    @FXML
    public void deployASServer(ActionEvent mouseEvent) {
        Platform.runLater(() -> {
            FXApp.infoFromTables.hostname = ASDeployHostBox.getSelectionModel().getSelectedItem();

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("AS Deploy Confirmation");
            alert.setHeaderText("Are you sure want to deploy " + FXApp.infoFromTables.asTempateName + " to " + FXApp.infoFromTables.hostname);
            alert.setContentText(new LocalUtils().getTextInfoAboutHost(FXApp.infoFromTables.hostname));

            Optional<ButtonType> option = alert.showAndWait();
            if (option.get() == ButtonType.OK) {
                logger.info(FXApp.infoFromTables.hostname + " " + FXApp.infoFromTables.asTempateName + "\n"
                        + FXApp.infoFromTables.serverPlugins + "\n" + FXApp.infoFromTables.clientPlugins);

                ASWorker asWorker = new ASWorker(FXApp.infoFromTables);
                if (asFileChosen) {
                    asWorker.setFileToDeploy(asFileToDeploy);
                }
                asWorker.start();
            }
        });
    }

    @FXML
    public void getGFApps(ActionEvent event) {
        new Thread(() -> {
            VMControlWorker vmControlWorker = new VMControlWorker(WebDeployHostBox.getSelectionModel().getSelectedItem(), 6);
            vmControlWorker.start();
            try {
                vmControlWorker.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Platform.runLater(() -> {
                JFXDialogLayout content = new JFXDialogLayout();
                Text text = new Text(vmControlWorker.getResult());
                text.setFont(new Font("Arial", 14));
                content.setHeading(new Text(WebDeployHostBox.getSelectionModel().getSelectedItem()));
                content.setBody(text);
                JFXDialog dialog = new JFXDialog(rootStackPane, content, JFXDialog.DialogTransition.CENTER);
                JFXButton button = new JFXButton("close");
                button.setOnAction(event1 -> dialog.close());
                content.setActions(button);
                dialog.toFront();
                dialog.show();
            });
            vmControlWorker.interrupt();
        }).start();
    }

    @FXML
    public void openDefaultPlugins(ActionEvent actionEvent) {
        Parent root;
        ArrayList<JFXCheckBox> temper = new ArrayList<>();
        try {
            FXMLLoader loader = new FXMLLoader();
            root = loader.load(getClass().getResource("/fxmls/Plugins.fxml"));
            pluginsController = loader.getController();

            temper.add((JFXCheckBox) root.lookup("#BookTabs"));
            temper.add((JFXCheckBox) root.lookup("#BookTabsClient"));
            temper.add((JFXCheckBox) root.lookup("#CatalogueExport"));
            temper.add((JFXCheckBox) root.lookup("#CatalogueExportClient"));
            temper.add((JFXCheckBox) root.lookup("#Comments"));
            temper.add((JFXCheckBox) root.lookup("#Duplicates"));
            temper.add((JFXCheckBox) root.lookup("#DuplicatesClient"));
            temper.add((JFXCheckBox) root.lookup("#EbsConfiguration"));
            temper.add((JFXCheckBox) root.lookup("#expiring_report_server"));
            temper.add((JFXCheckBox) root.lookup("#FastInput"));
            temper.add((JFXCheckBox) root.lookup("#FastInputClient"));
            temper.add((JFXCheckBox) root.lookup("#Favorites"));
            temper.add((JFXCheckBox) root.lookup("#fill_report_server"));
            temper.add((JFXCheckBox) root.lookup("#fund_report_server"));
            temper.add((JFXCheckBox) root.lookup("#PublicationsOffline"));
            temper.add((JFXCheckBox) root.lookup("#PublicationsOfflineClient"));
            temper.add((JFXCheckBox) root.lookup("#MassInheritance"));
            temper.add((JFXCheckBox) root.lookup("#MGOK"));
            temper.add((JFXCheckBox) root.lookup("#registration_server"));
            temper.add((JFXCheckBox) root.lookup("#registration_client"));
            temper.add((JFXCheckBox) root.lookup("#reports_server"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearch"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearchClient"));
            temper.add((JFXCheckBox) root.lookup("#usage_report_server"));
            temper.add((JFXCheckBox) root.lookup("#UserData"));
            temper.add((JFXCheckBox) root.lookup("#ViewerCopies"));
            temper.add((JFXCheckBox) root.lookup("#ViewerLink"));
            temper.add((JFXCheckBox) root.lookup("#WorkflowV2"));
            temper.add((JFXCheckBox) root.lookup("#WorkflowClientV2"));
            temper.add((JFXCheckBox) root.lookup("#xml_export_server"));
            temper.add((JFXCheckBox) root.lookup("#av_excel_import_client"));

            for (JFXCheckBox check : temper) {
                check.setSelected(true);
            }

            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Default");
            stage.setScene(new Scene(root));
            stage.show();

            FXApp.infoFromTables.asTempateName = "Default";

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void openEBSPlugins(ActionEvent mouseEvent) {
        Parent root;
        ArrayList<JFXCheckBox> temper = new ArrayList<>();
        try {
            root = FXMLLoader.load(getClass().getResource("/fxmls/Plugins.fxml"));

            temper.add((JFXCheckBox) root.lookup("#BookTabs"));
            temper.add((JFXCheckBox) root.lookup("#BookTabsClient"));
            temper.add((JFXCheckBox) root.lookup("#Comments"));
            temper.add((JFXCheckBox) root.lookup("#DIPImport"));
            temper.add((JFXCheckBox) root.lookup("#DIPImportClient"));
            temper.add((JFXCheckBox) root.lookup("#EbsConfiguration"));
            temper.add((JFXCheckBox) root.lookup("#FastInput"));
            temper.add((JFXCheckBox) root.lookup("#Favorites"));
            temper.add((JFXCheckBox) root.lookup("#fill_report_server"));
            temper.add((JFXCheckBox) root.lookup("#FullPreviewFiles"));
            temper.add((JFXCheckBox) root.lookup("#fund_report_server"));
            temper.add((JFXCheckBox) root.lookup("#HelpPlugin"));
            temper.add((JFXCheckBox) root.lookup("#PublicFileAccess"));
            temper.add((JFXCheckBox) root.lookup("#registration_server"));
            temper.add((JFXCheckBox) root.lookup("#registration_client"));
            temper.add((JFXCheckBox) root.lookup("#reports_server"));
            temper.add((JFXCheckBox) root.lookup("#PublicationsOfflineClient"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearch"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearchClient"));
            temper.add((JFXCheckBox) root.lookup("#usage_report_server"));
            temper.add((JFXCheckBox) root.lookup("#UserData"));
            temper.add((JFXCheckBox) root.lookup("#ViewerCopies"));

            for (JFXCheckBox check : temper) {
                check.setSelected(true);
            }

            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Ebs");
            stage.setScene(new Scene(root));
            stage.show();

            FXApp.infoFromTables.asTempateName = "EBS";
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void openMCHSPlugins(ActionEvent mouseEvent) {
        Parent root;
        ArrayList<JFXCheckBox> temper = new ArrayList<>();
        try {
            root = FXMLLoader.load(getClass().getResource("/fxmls/Plugins.fxml"));

            temper.add((JFXCheckBox) root.lookup("#BookTabs"));
            temper.add((JFXCheckBox) root.lookup("#Comments"));
            temper.add((JFXCheckBox) root.lookup("#EbsConfiguration"));
            temper.add((JFXCheckBox) root.lookup("#Favorites"));
            temper.add((JFXCheckBox) root.lookup("#fill_report_server"));
            temper.add((JFXCheckBox) root.lookup("#FullPreviewFiles"));
            temper.add((JFXCheckBox) root.lookup("#fund_report_server"));
            temper.add((JFXCheckBox) root.lookup("#HelpPlugin"));
            temper.add((JFXCheckBox) root.lookup("#PublicFileAccess"));
            temper.add((JFXCheckBox) root.lookup("#registration_server"));
            temper.add((JFXCheckBox) root.lookup("#registration_client"));
            temper.add((JFXCheckBox) root.lookup("#reports_server"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearch"));
            temper.add((JFXCheckBox) root.lookup("#synchronization_server"));
            temper.add((JFXCheckBox) root.lookup("#usage_report_server"));
            temper.add((JFXCheckBox) root.lookup("#UserData"));
            temper.add((JFXCheckBox) root.lookup("#ViewerCopies"));

            for (JFXCheckBox check : temper) {
                check.setSelected(true);
            }

            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("MCHS");
            stage.setScene(new Scene(root));
            stage.show();

            FXApp.infoFromTables.asTempateName = "MCHS";
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void openHelicoptersPlugins(ActionEvent mouseEvent) {
        Parent root;
        ArrayList<JFXCheckBox> temper = new ArrayList<>();
        try {
            root = FXMLLoader.load(getClass().getResource("/fxmls/Plugins.fxml"));

            temper.add((JFXCheckBox) root.lookup("#BookTabs"));
            temper.add((JFXCheckBox) root.lookup("#BookTabsClient"));
            temper.add((JFXCheckBox) root.lookup("#CatalogueExport"));
            temper.add((JFXCheckBox) root.lookup("#CatalogueExportClient"));
            temper.add((JFXCheckBox) root.lookup("#Comments"));
            temper.add((JFXCheckBox) root.lookup("#ConsideredCopiesClient"));
            temper.add((JFXCheckBox) root.lookup("#ConsideredCopies"));
            temper.add((JFXCheckBox) root.lookup("#custom_considered_copies_report_server"));
            temper.add((JFXCheckBox) root.lookup("#custom_nd_change_report_server"));
            temper.add((JFXCheckBox) root.lookup("#custom_subscriptions_report_server"));
            temper.add((JFXCheckBox) root.lookup("#Duplicates"));
            temper.add((JFXCheckBox) root.lookup("#DuplicatesClient"));
            temper.add((JFXCheckBox) root.lookup("#EbsConfiguration"));
            temper.add((JFXCheckBox) root.lookup("#expiring_report_server"));
            temper.add((JFXCheckBox) root.lookup("#FastInput"));
            temper.add((JFXCheckBox) root.lookup("#FastInputClient"));
            temper.add((JFXCheckBox) root.lookup("#Favorites"));
            temper.add((JFXCheckBox) root.lookup("#fill_report_server"));
            temper.add((JFXCheckBox) root.lookup("#file_binder_client"));
            temper.add((JFXCheckBox) root.lookup("#file_binder_server"));
            temper.add((JFXCheckBox) root.lookup("#fund_report_server"));
            temper.add((JFXCheckBox) root.lookup("#linked_report_server"));
            temper.add((JFXCheckBox) root.lookup("#nd_changes_server"));
            temper.add((JFXCheckBox) root.lookup("#ND_subscribe"));
            temper.add((JFXCheckBox) root.lookup("#ND_subscribeClient"));
            temper.add((JFXCheckBox) root.lookup("#NormativeControl"));
            temper.add((JFXCheckBox) root.lookup("#NormativeControlClient"));
            temper.add((JFXCheckBox) root.lookup("#PublicationsOffline"));
            temper.add((JFXCheckBox) root.lookup("#PublicationsOfflineClient"));
            temper.add((JFXCheckBox) root.lookup("#MassInheritance"));
            temper.add((JFXCheckBox) root.lookup("#reports_server"));
            temper.add((JFXCheckBox) root.lookup("#Reviews"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearch"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearchClient"));
            temper.add((JFXCheckBox) root.lookup("#stubfile_server"));
            temper.add((JFXCheckBox) root.lookup("#stubfile_client"));
            temper.add((JFXCheckBox) root.lookup("#usage_report_server"));
            temper.add((JFXCheckBox) root.lookup("#UserData"));
            temper.add((JFXCheckBox) root.lookup("#ViewerCopies"));
            temper.add((JFXCheckBox) root.lookup("#ViewerLink"));
            temper.add((JFXCheckBox) root.lookup("#Voting"));
            temper.add((JFXCheckBox) root.lookup("#WorkflowV1"));
            temper.add((JFXCheckBox) root.lookup("#WorkflowClientV1"));
            temper.add((JFXCheckBox) root.lookup("#xml_export_server"));
            temper.add((JFXCheckBox) root.lookup("#file_binder_server"));

            for (JFXCheckBox check : temper) {
                check.setSelected(true);
            }

            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Helicopters");
            stage.setScene(new Scene(root));
            stage.show();
            FXApp.infoFromTables.asTempateName = "Helicopters";
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void openOMKPlugins(ActionEvent mouseEvent) {
        Parent root;
        ArrayList<JFXCheckBox> temper = new ArrayList<>();
        try {
            root = FXMLLoader.load(getClass().getResource("/fxmls/Plugins.fxml"));

            temper.add((JFXCheckBox) root.lookup("#BookTabs"));
            temper.add((JFXCheckBox) root.lookup("#BookTabsClient"));
            temper.add((JFXCheckBox) root.lookup("#CatalogueExport"));
            temper.add((JFXCheckBox) root.lookup("#CatalogueExportClient"));
            temper.add((JFXCheckBox) root.lookup("#Comments"));
            temper.add((JFXCheckBox) root.lookup("#ConsideredCopiesClient"));
            temper.add((JFXCheckBox) root.lookup("#ConsideredCopies"));
            temper.add((JFXCheckBox) root.lookup("#custom_considered_copies_report_server"));
            temper.add((JFXCheckBox) root.lookup("#custom_nd_change_report_server"));
            temper.add((JFXCheckBox) root.lookup("#custom_subscriptions_report_server"));
            temper.add((JFXCheckBox) root.lookup("#EbsConfiguration"));
            temper.add((JFXCheckBox) root.lookup("#expiring_report_server"));
            temper.add((JFXCheckBox) root.lookup("#Favorites"));
            temper.add((JFXCheckBox) root.lookup("#fund_report_server"));
            temper.add((JFXCheckBox) root.lookup("#nd_changes_server"));
            temper.add((JFXCheckBox) root.lookup("#ND_subscribe"));
            temper.add((JFXCheckBox) root.lookup("#ND_subscribeClient"));
            temper.add((JFXCheckBox) root.lookup("#NormativeControl"));
            temper.add((JFXCheckBox) root.lookup("#NormativeControlClient"));
            temper.add((JFXCheckBox) root.lookup("#omk_extension_server"));
            temper.add((JFXCheckBox) root.lookup("#omk_wss_server"));
            temper.add((JFXCheckBox) root.lookup("#PublicationsOffline"));
            temper.add((JFXCheckBox) root.lookup("#PublicationsOfflineClient"));
            temper.add((JFXCheckBox) root.lookup("#reports_server"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearch"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearchClient"));
            temper.add((JFXCheckBox) root.lookup("#usage_report_server"));
            temper.add((JFXCheckBox) root.lookup("#UserData"));
            temper.add((JFXCheckBox) root.lookup("#WorkflowV2"));
            temper.add((JFXCheckBox) root.lookup("#WorkflowClientV2"));
            temper.add((JFXCheckBox) root.lookup("#considered_copies_workflow_server"));
            temper.add((JFXCheckBox) root.lookup("#considered_copies_workflow_client"));

            for (JFXCheckBox check : temper) {
                check.setSelected(true);
            }

            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("OMK");
            stage.setScene(new Scene(root));
            stage.show();

            FXApp.infoFromTables.asTempateName = "OMK";
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void openCQMSPlugins(ActionEvent mouseEvent) {
        Parent root;
        ArrayList<JFXCheckBox> temper = new ArrayList<>();
        try {
            root = FXMLLoader.load(getClass().getResource("/fxmls/Plugins.fxml"));

            temper.add((JFXCheckBox) root.lookup("#BookTabs"));
            temper.add((JFXCheckBox) root.lookup("#BookTabsClient"));
            temper.add((JFXCheckBox) root.lookup("#CatalogueExport"));
            temper.add((JFXCheckBox) root.lookup("#CatalogueExportClient"));
            temper.add((JFXCheckBox) root.lookup("#Comments"));
            temper.add((JFXCheckBox) root.lookup("#ConsideredCopiesClient"));
            temper.add((JFXCheckBox) root.lookup("#ConsideredCopies"));
            temper.add((JFXCheckBox) root.lookup("#cqms_audits_server"));
            temper.add((JFXCheckBox) root.lookup("#cqms_fixation_base_crm_server"));
            temper.add((JFXCheckBox) root.lookup("#cqms_fixation_base_server"));
            temper.add((JFXCheckBox) root.lookup("#cqms_monitoring_server"));
            temper.add((JFXCheckBox) root.lookup("#cqms_risks_server"));
            temper.add((JFXCheckBox) root.lookup("#custom_considered_copies_report_server"));
            temper.add((JFXCheckBox) root.lookup("#custom_nd_change_report_server"));
            temper.add((JFXCheckBox) root.lookup("#custom_subscriptions_report_server"));
            temper.add((JFXCheckBox) root.lookup("#Duplicates"));
            temper.add((JFXCheckBox) root.lookup("#DuplicatesClient"));
            temper.add((JFXCheckBox) root.lookup("#EbsConfiguration"));
            temper.add((JFXCheckBox) root.lookup("#expiring_report_server"));
            temper.add((JFXCheckBox) root.lookup("#FastInput"));
            temper.add((JFXCheckBox) root.lookup("#FastInputClient"));
            temper.add((JFXCheckBox) root.lookup("#Favorites"));
            temper.add((JFXCheckBox) root.lookup("#fill_report_server"));
            temper.add((JFXCheckBox) root.lookup("#fund_report_server"));
            temper.add((JFXCheckBox) root.lookup("#nd_changes_server"));
            temper.add((JFXCheckBox) root.lookup("#ND_subscribe"));
            temper.add((JFXCheckBox) root.lookup("#ND_subscribeClient"));
            temper.add((JFXCheckBox) root.lookup("#NormativeControl"));
            temper.add((JFXCheckBox) root.lookup("#NormativeControlClient"));
            temper.add((JFXCheckBox) root.lookup("#MassInheritance"));
            temper.add((JFXCheckBox) root.lookup("#PublicationsOffline"));
            temper.add((JFXCheckBox) root.lookup("#PublicationsOfflineClient"));
            temper.add((JFXCheckBox) root.lookup("#registration_client"));
            temper.add((JFXCheckBox) root.lookup("#registration_server"));
            temper.add((JFXCheckBox) root.lookup("#reports_server"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearch"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearchClient"));
            temper.add((JFXCheckBox) root.lookup("#usage_report_server"));
            temper.add((JFXCheckBox) root.lookup("#UserData"));
            temper.add((JFXCheckBox) root.lookup("#ViewerCopies"));
            temper.add((JFXCheckBox) root.lookup("#ViewerLink"));
            temper.add((JFXCheckBox) root.lookup("#WorkflowV2"));
            temper.add((JFXCheckBox) root.lookup("#WorkflowClientV2"));
            temper.add((JFXCheckBox) root.lookup("#xml_export_server"));

            for (JFXCheckBox check : temper) {
                check.setSelected(true);
            }

            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("CQMS");
            stage.setScene(new Scene(root));
            stage.show();

            FXApp.infoFromTables.asTempateName = "CQMS";
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void openKBNTIPlugins(ActionEvent mouseEvent) {
        Parent root;
        ArrayList<JFXCheckBox> temper = new ArrayList<>();
        try {
            root = FXMLLoader.load(getClass().getResource("/fxmls/Plugins.fxml"));

            temper.add((JFXCheckBox) root.lookup("#BookTabs"));
            temper.add((JFXCheckBox) root.lookup("#BookTabsClient"));
            temper.add((JFXCheckBox) root.lookup("#CatalogueExport"));
            temper.add((JFXCheckBox) root.lookup("#CatalogueExportClient"));
            temper.add((JFXCheckBox) root.lookup("#Comments"));
            temper.add((JFXCheckBox) root.lookup("#ConsideredCopiesClient"));
            temper.add((JFXCheckBox) root.lookup("#ConsideredCopies"));
            temper.add((JFXCheckBox) root.lookup("#custom_considered_copies_report_server"));
            temper.add((JFXCheckBox) root.lookup("#custom_nd_change_report_server"));
            temper.add((JFXCheckBox) root.lookup("#custom_subscriptions_report_server"));
            temper.add((JFXCheckBox) root.lookup("#EbsConfiguration"));
            temper.add((JFXCheckBox) root.lookup("#expiring_report_server"));
            temper.add((JFXCheckBox) root.lookup("#Favorites"));
            temper.add((JFXCheckBox) root.lookup("#fund_report_server"));
            temper.add((JFXCheckBox) root.lookup("#kbntind_extension_server"));
            temper.add((JFXCheckBox) root.lookup("#nd_changes_server"));
            temper.add((JFXCheckBox) root.lookup("#ND_subscribe"));
            temper.add((JFXCheckBox) root.lookup("#ND_subscribeClient"));
            temper.add((JFXCheckBox) root.lookup("#NormativeControl"));
            temper.add((JFXCheckBox) root.lookup("#NormativeControlClient"));
            temper.add((JFXCheckBox) root.lookup("#onlyoffice_server"));
            temper.add((JFXCheckBox) root.lookup("#PublicationsOffline"));
            temper.add((JFXCheckBox) root.lookup("#PublicationsOfflineClient"));
            temper.add((JFXCheckBox) root.lookup("#reports_server"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearch"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearchClient"));
            temper.add((JFXCheckBox) root.lookup("#usage_report_server"));
            temper.add((JFXCheckBox) root.lookup("#UserData"));
            temper.add((JFXCheckBox) root.lookup("#WorkflowV2"));
            temper.add((JFXCheckBox) root.lookup("#WorkflowClientV2"));
            temper.add((JFXCheckBox) root.lookup("#considered_copies_workflow_server"));
            temper.add((JFXCheckBox) root.lookup("#considered_copies_workflow_client"));

            for (JFXCheckBox check : temper) {
                if (check != null)
                    check.setSelected(true);
            }

            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("KBNTIND");
            stage.setScene(new Scene(root));
            stage.show();

            FXApp.infoFromTables.asTempateName = "KBNTIND";
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void openClearPlugins(ActionEvent mouseEvent) {
        Parent root;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxmls/Plugins.fxml"));
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Clean");
            stage.setScene(new Scene(root));
            stage.show();

            FXApp.infoFromTables.asTempateName = "Clean";
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void setDefaultPlugins() {
        Parent root;
        ArrayList<JFXCheckBox> temper = new ArrayList<>();
        try {
            FXMLLoader loader = new FXMLLoader();
            root = loader.load(getClass().getResource("/fxmls/Plugins.fxml"));
            pluginsController = loader.getController();

            temper.add((JFXCheckBox) root.lookup("#BookTabs"));
            temper.add((JFXCheckBox) root.lookup("#BookTabsClient"));
            temper.add((JFXCheckBox) root.lookup("#CatalogueExport"));
            temper.add((JFXCheckBox) root.lookup("#CatalogueExportClient"));
            temper.add((JFXCheckBox) root.lookup("#Comments"));
            temper.add((JFXCheckBox) root.lookup("#Duplicates"));
            temper.add((JFXCheckBox) root.lookup("#DuplicatesClient"));
            temper.add((JFXCheckBox) root.lookup("#EbsConfiguration"));
            temper.add((JFXCheckBox) root.lookup("#expiring_report_server"));
            temper.add((JFXCheckBox) root.lookup("#FastInput"));
            temper.add((JFXCheckBox) root.lookup("#FastInputClient"));
            temper.add((JFXCheckBox) root.lookup("#Favorites"));
            temper.add((JFXCheckBox) root.lookup("#fill_report_server"));
            temper.add((JFXCheckBox) root.lookup("#fund_report_server"));
            temper.add((JFXCheckBox) root.lookup("#PublicationsOffline"));
            temper.add((JFXCheckBox) root.lookup("#PublicationsOfflineClient"));
            temper.add((JFXCheckBox) root.lookup("#MassInheritance"));
            temper.add((JFXCheckBox) root.lookup("#MGOK"));
            temper.add((JFXCheckBox) root.lookup("#registration_server"));
            temper.add((JFXCheckBox) root.lookup("#registration_client"));
            temper.add((JFXCheckBox) root.lookup("#reports_server"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearch"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearchClient"));
            temper.add((JFXCheckBox) root.lookup("#usage_report_server"));
            temper.add((JFXCheckBox) root.lookup("#UserData"));
            temper.add((JFXCheckBox) root.lookup("#ViewerCopies"));
            temper.add((JFXCheckBox) root.lookup("#ViewerLink"));
            temper.add((JFXCheckBox) root.lookup("#WorkflowV2"));
            temper.add((JFXCheckBox) root.lookup("#WorkflowClientV2"));
            temper.add((JFXCheckBox) root.lookup("#xml_export_server"));
            temper.add((JFXCheckBox) root.lookup("#av_excel_import_client"));

            for (JFXCheckBox check : temper) {
                check.setSelected(true);
            }

            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Default");
            stage.setScene(new Scene(root));

            JFXButton accept = (JFXButton) root.lookup("#acceptPlugins");
            accept.fire();

            FXApp.infoFromTables.asTempateName = "Deafult";
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void setEbsPlugins() {
        Parent root;
        ArrayList<JFXCheckBox> temper = new ArrayList<>();
        try {
            root = FXMLLoader.load(getClass().getResource("/fxmls/Plugins.fxml"));

            temper.add((JFXCheckBox) root.lookup("#BookTabs"));
            temper.add((JFXCheckBox) root.lookup("#BookTabsClient"));
            temper.add((JFXCheckBox) root.lookup("#Comments"));
            temper.add((JFXCheckBox) root.lookup("#DIPImport"));
            temper.add((JFXCheckBox) root.lookup("#DIPImportClient"));
            temper.add((JFXCheckBox) root.lookup("#EbsConfiguration"));
            temper.add((JFXCheckBox) root.lookup("#FastInput"));
            temper.add((JFXCheckBox) root.lookup("#Favorites"));
            temper.add((JFXCheckBox) root.lookup("#fill_report_server"));
            temper.add((JFXCheckBox) root.lookup("#FullPreviewFiles"));
            temper.add((JFXCheckBox) root.lookup("#fund_report_server"));
            temper.add((JFXCheckBox) root.lookup("#HelpPlugin"));
            temper.add((JFXCheckBox) root.lookup("#PublicFileAccess"));
            temper.add((JFXCheckBox) root.lookup("#registration_server"));
            temper.add((JFXCheckBox) root.lookup("#registration_client"));
            temper.add((JFXCheckBox) root.lookup("#reports_server"));
            temper.add((JFXCheckBox) root.lookup("#PublicationsOfflineClient"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearch"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearchClient"));
            temper.add((JFXCheckBox) root.lookup("#usage_report_server"));
            temper.add((JFXCheckBox) root.lookup("#UserData"));
            temper.add((JFXCheckBox) root.lookup("#ViewerCopies"));

            for (JFXCheckBox check : temper) {
                check.setSelected(true);
            }

            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("EBS");
            stage.setScene(new Scene(root));

            JFXButton accept = (JFXButton) root.lookup("#acceptPlugins");
            accept.fire();

            FXApp.infoFromTables.asTempateName = "EBS";
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void setMchsPlugins() {
        Parent root;
        ArrayList<JFXCheckBox> temper = new ArrayList<>();
        try {
            root = FXMLLoader.load(getClass().getResource("/fxmls/Plugins.fxml"));

            temper.add((JFXCheckBox) root.lookup("#BookTabs"));
            temper.add((JFXCheckBox) root.lookup("#Comments"));
            temper.add((JFXCheckBox) root.lookup("#EbsConfiguration"));
            temper.add((JFXCheckBox) root.lookup("#Favorites"));
            temper.add((JFXCheckBox) root.lookup("#fill_report_server"));
            temper.add((JFXCheckBox) root.lookup("#FullPreviewFiles"));
            temper.add((JFXCheckBox) root.lookup("#fund_report_server"));
            temper.add((JFXCheckBox) root.lookup("#HelpPlugin"));
            temper.add((JFXCheckBox) root.lookup("#PublicFileAccess"));
            temper.add((JFXCheckBox) root.lookup("#registration_server"));
            temper.add((JFXCheckBox) root.lookup("#registration_client"));
            temper.add((JFXCheckBox) root.lookup("#reports_server"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearch"));
            temper.add((JFXCheckBox) root.lookup("#synchronization_server"));
            temper.add((JFXCheckBox) root.lookup("#usage_report_server"));
            temper.add((JFXCheckBox) root.lookup("#UserData"));
            temper.add((JFXCheckBox) root.lookup("#ViewerCopies"));

            for (JFXCheckBox check : temper) {
                check.setSelected(true);
            }

            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("MCHS");
            stage.setScene(new Scene(root));

            JFXButton accept = (JFXButton) root.lookup("#acceptPlugins");
            accept.fire();

            FXApp.infoFromTables.asTempateName = "MCHS";
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void setHelicoptersPlugins() {
        Parent root;
        ArrayList<JFXCheckBox> temper = new ArrayList<>();
        try {
            root = FXMLLoader.load(getClass().getResource("/fxmls/Plugins.fxml"));

            temper.add((JFXCheckBox) root.lookup("#BookTabs"));
            temper.add((JFXCheckBox) root.lookup("#BookTabsClient"));
            temper.add((JFXCheckBox) root.lookup("#CatalogueExport"));
            temper.add((JFXCheckBox) root.lookup("#CatalogueExportClient"));
            temper.add((JFXCheckBox) root.lookup("#Comments"));
            temper.add((JFXCheckBox) root.lookup("#ConsideredCopiesClient"));
            temper.add((JFXCheckBox) root.lookup("#ConsideredCopies"));
            temper.add((JFXCheckBox) root.lookup("#custom_considered_copies_report_server"));
            temper.add((JFXCheckBox) root.lookup("#custom_nd_change_report_server"));
            temper.add((JFXCheckBox) root.lookup("#custom_subscriptions_report_server"));
            temper.add((JFXCheckBox) root.lookup("#Duplicates"));
            temper.add((JFXCheckBox) root.lookup("#DuplicatesClient"));
            temper.add((JFXCheckBox) root.lookup("#EbsConfiguration"));
            temper.add((JFXCheckBox) root.lookup("#expiring_report_server"));
            temper.add((JFXCheckBox) root.lookup("#FastInput"));
            temper.add((JFXCheckBox) root.lookup("#FastInputClient"));
            temper.add((JFXCheckBox) root.lookup("#Favorites"));
            temper.add((JFXCheckBox) root.lookup("#fill_report_server"));
            temper.add((JFXCheckBox) root.lookup("#file_binder_client"));
            temper.add((JFXCheckBox) root.lookup("#file_binder_server"));
            temper.add((JFXCheckBox) root.lookup("#fund_report_server"));
            temper.add((JFXCheckBox) root.lookup("#linked_report_server"));
            temper.add((JFXCheckBox) root.lookup("#nd_changes_server"));
            temper.add((JFXCheckBox) root.lookup("#ND_subscribe"));
            temper.add((JFXCheckBox) root.lookup("#ND_subscribeClient"));
            temper.add((JFXCheckBox) root.lookup("#NormativeControl"));
            temper.add((JFXCheckBox) root.lookup("#NormativeControlClient"));
            temper.add((JFXCheckBox) root.lookup("#PublicationsOffline"));
            temper.add((JFXCheckBox) root.lookup("#PublicationsOfflineClient"));
            temper.add((JFXCheckBox) root.lookup("#MassInheritance"));
            temper.add((JFXCheckBox) root.lookup("#reports_server"));
            temper.add((JFXCheckBox) root.lookup("#Reviews"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearch"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearchClient"));
            temper.add((JFXCheckBox) root.lookup("#stubfile_server"));
            temper.add((JFXCheckBox) root.lookup("#stubfile_client"));
            temper.add((JFXCheckBox) root.lookup("#usage_report_server"));
            temper.add((JFXCheckBox) root.lookup("#UserData"));
            temper.add((JFXCheckBox) root.lookup("#ViewerCopies"));
            temper.add((JFXCheckBox) root.lookup("#ViewerLink"));
            temper.add((JFXCheckBox) root.lookup("#Voting"));
            temper.add((JFXCheckBox) root.lookup("#WorkflowV1"));
            temper.add((JFXCheckBox) root.lookup("#WorkflowClientV1"));
            temper.add((JFXCheckBox) root.lookup("#xml_export_server"));
            temper.add((JFXCheckBox) root.lookup("#file_binder_server"));

            for (JFXCheckBox check : temper) {
                check.setSelected(true);
            }

            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Helicopters");
            stage.setScene(new Scene(root));

            JFXButton accept = (JFXButton) root.lookup("#acceptPlugins");
            accept.fire();

            FXApp.infoFromTables.asTempateName = "Helicopters";
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void setOMKPlugins() {
        Parent root;
        ArrayList<JFXCheckBox> temper = new ArrayList<>();
        try {
            root = FXMLLoader.load(getClass().getResource("/fxmls/Plugins.fxml"));

            temper.add((JFXCheckBox) root.lookup("#BookTabs"));
            temper.add((JFXCheckBox) root.lookup("#BookTabsClient"));
            temper.add((JFXCheckBox) root.lookup("#CatalogueExport"));
            temper.add((JFXCheckBox) root.lookup("#CatalogueExportClient"));
            temper.add((JFXCheckBox) root.lookup("#Comments"));
            temper.add((JFXCheckBox) root.lookup("#ConsideredCopiesClient"));
            temper.add((JFXCheckBox) root.lookup("#ConsideredCopies"));
            temper.add((JFXCheckBox) root.lookup("#custom_considered_copies_report_server"));
            temper.add((JFXCheckBox) root.lookup("#custom_nd_change_report_server"));
            temper.add((JFXCheckBox) root.lookup("#custom_subscriptions_report_server"));
            temper.add((JFXCheckBox) root.lookup("#EbsConfiguration"));
            temper.add((JFXCheckBox) root.lookup("#expiring_report_server"));
            temper.add((JFXCheckBox) root.lookup("#fund_report_server"));
            temper.add((JFXCheckBox) root.lookup("#Favorites"));
            temper.add((JFXCheckBox) root.lookup("#nd_changes_server"));
            temper.add((JFXCheckBox) root.lookup("#ND_subscribe"));
            temper.add((JFXCheckBox) root.lookup("#ND_subscribeClient"));
            temper.add((JFXCheckBox) root.lookup("#NormativeControl"));
            temper.add((JFXCheckBox) root.lookup("#NormativeControlClient"));
            temper.add((JFXCheckBox) root.lookup("#omk_extension_server"));
            temper.add((JFXCheckBox) root.lookup("#omk_wss_server"));
            temper.add((JFXCheckBox) root.lookup("#PublicationsOffline"));
            temper.add((JFXCheckBox) root.lookup("#PublicationsOfflineClient"));
            temper.add((JFXCheckBox) root.lookup("#reports_server"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearch"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearchClient"));
            temper.add((JFXCheckBox) root.lookup("#usage_report_server"));
            temper.add((JFXCheckBox) root.lookup("#UserData"));
            temper.add((JFXCheckBox) root.lookup("#WorkflowV2"));
            temper.add((JFXCheckBox) root.lookup("#WorkflowClientV2"));
            temper.add((JFXCheckBox) root.lookup("#considered_copies_workflow_server"));
            temper.add((JFXCheckBox) root.lookup("#considered_copies_workflow_client"));

            for (JFXCheckBox check : temper) {
                check.setSelected(true);
            }

            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("OMK");
            stage.setScene(new Scene(root));

            JFXButton accept = (JFXButton) root.lookup("#acceptPlugins");
            accept.fire();

            FXApp.infoFromTables.asTempateName = "OMK";
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void setCQMSPlugins() {
        Parent root;
        ArrayList<JFXCheckBox> temper = new ArrayList<>();
        try {
            root = FXMLLoader.load(getClass().getResource("/fxmls/Plugins.fxml"));

            temper.add((JFXCheckBox) root.lookup("#BookTabs"));
            temper.add((JFXCheckBox) root.lookup("#BookTabsClient"));
            temper.add((JFXCheckBox) root.lookup("#CatalogueExport"));
            temper.add((JFXCheckBox) root.lookup("#CatalogueExportClient"));
            temper.add((JFXCheckBox) root.lookup("#Comments"));
            temper.add((JFXCheckBox) root.lookup("#ConsideredCopiesClient"));
            temper.add((JFXCheckBox) root.lookup("#ConsideredCopies"));
            temper.add((JFXCheckBox) root.lookup("#cqms_audits_server"));
            temper.add((JFXCheckBox) root.lookup("#cqms_fixation_base_crm_server"));
            temper.add((JFXCheckBox) root.lookup("#cqms_fixation_base_server"));
            temper.add((JFXCheckBox) root.lookup("#cqms_monitoring_server"));
            temper.add((JFXCheckBox) root.lookup("#cqms_risks_server"));
            temper.add((JFXCheckBox) root.lookup("#custom_considered_copies_report_server"));
            temper.add((JFXCheckBox) root.lookup("#custom_nd_change_report_server"));
            temper.add((JFXCheckBox) root.lookup("#custom_subscriptions_report_server"));
            temper.add((JFXCheckBox) root.lookup("#Duplicates"));
            temper.add((JFXCheckBox) root.lookup("#DuplicatesClient"));
            temper.add((JFXCheckBox) root.lookup("#EbsConfiguration"));
            temper.add((JFXCheckBox) root.lookup("#expiring_report_server"));
            temper.add((JFXCheckBox) root.lookup("#FastInput"));
            temper.add((JFXCheckBox) root.lookup("#FastInputClient"));
            temper.add((JFXCheckBox) root.lookup("#Favorites"));
            temper.add((JFXCheckBox) root.lookup("#fill_report_server"));
            temper.add((JFXCheckBox) root.lookup("#fund_report_server"));
            temper.add((JFXCheckBox) root.lookup("#nd_changes_server"));
            temper.add((JFXCheckBox) root.lookup("#ND_subscribe"));
            temper.add((JFXCheckBox) root.lookup("#ND_subscribeClient"));
            temper.add((JFXCheckBox) root.lookup("#NormativeControl"));
            temper.add((JFXCheckBox) root.lookup("#NormativeControlClient"));
            temper.add((JFXCheckBox) root.lookup("#MassInheritance"));
            temper.add((JFXCheckBox) root.lookup("#PublicationsOffline"));
            temper.add((JFXCheckBox) root.lookup("#PublicationsOfflineClient"));
            temper.add((JFXCheckBox) root.lookup("#registration_client"));
            temper.add((JFXCheckBox) root.lookup("#registration_server"));
            temper.add((JFXCheckBox) root.lookup("#reports_server"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearch"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearchClient"));
            temper.add((JFXCheckBox) root.lookup("#usage_report_server"));
            temper.add((JFXCheckBox) root.lookup("#UserData"));
            temper.add((JFXCheckBox) root.lookup("#ViewerCopies"));
            temper.add((JFXCheckBox) root.lookup("#ViewerLink"));
            temper.add((JFXCheckBox) root.lookup("#WorkflowV2"));
            temper.add((JFXCheckBox) root.lookup("#WorkflowClientV2"));
            temper.add((JFXCheckBox) root.lookup("#xml_export_server"));

            for (JFXCheckBox check : temper) {
                check.setSelected(true);
            }

            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("CQMS");
            stage.setScene(new Scene(root));

            JFXButton accept = (JFXButton) root.lookup("#acceptPlugins");
            accept.fire();

            FXApp.infoFromTables.asTempateName = "CQMS";
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void setKBNTIPlugins() {
        Parent root;
        ArrayList<JFXCheckBox> temper = new ArrayList<>();
        try {
            root = FXMLLoader.load(getClass().getResource("/fxmls/Plugins.fxml"));

            temper.add((JFXCheckBox) root.lookup("#BookTabs"));
            temper.add((JFXCheckBox) root.lookup("#BookTabsClient"));
            temper.add((JFXCheckBox) root.lookup("#CatalogueExport"));
            temper.add((JFXCheckBox) root.lookup("#CatalogueExportClient"));
            temper.add((JFXCheckBox) root.lookup("#Comments"));
            temper.add((JFXCheckBox) root.lookup("#ConsideredCopiesClient"));
            temper.add((JFXCheckBox) root.lookup("#ConsideredCopies"));
            temper.add((JFXCheckBox) root.lookup("#custom_considered_copies_report_server"));
            temper.add((JFXCheckBox) root.lookup("#custom_nd_change_report_server"));
            temper.add((JFXCheckBox) root.lookup("#custom_subscriptions_report_server"));
            temper.add((JFXCheckBox) root.lookup("#EbsConfiguration"));
            temper.add((JFXCheckBox) root.lookup("#expiring_report_server"));
            temper.add((JFXCheckBox) root.lookup("#Favorites"));
            temper.add((JFXCheckBox) root.lookup("#fund_report_server"));
            temper.add((JFXCheckBox) root.lookup("#onlyoffice_server"));
            temper.add((JFXCheckBox) root.lookup("#nd_changes_server"));
            temper.add((JFXCheckBox) root.lookup("#ND_subscribe"));
            temper.add((JFXCheckBox) root.lookup("#ND_subscribeClient"));
            temper.add((JFXCheckBox) root.lookup("#NormativeControl"));
            temper.add((JFXCheckBox) root.lookup("#NormativeControlClient"));
            temper.add((JFXCheckBox) root.lookup("#NormativeControlClient"));
            temper.add((JFXCheckBox) root.lookup("#PublicationsOffline"));
            temper.add((JFXCheckBox) root.lookup("#PublicationsOfflineClient"));
            temper.add((JFXCheckBox) root.lookup("#reports_server"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearch"));
            temper.add((JFXCheckBox) root.lookup("#SavedSearchClient"));
            temper.add((JFXCheckBox) root.lookup("#usage_report_server"));
            temper.add((JFXCheckBox) root.lookup("#UserData"));
            temper.add((JFXCheckBox) root.lookup("#WorkflowV2"));
            temper.add((JFXCheckBox) root.lookup("#WorkflowClientV2"));
            temper.add((JFXCheckBox) root.lookup("#considered_copies_workflow_server"));
            temper.add((JFXCheckBox) root.lookup("#considered_copies_workflow_client"));

            for (JFXCheckBox check : temper) {
                if (check != null)
                    check.setSelected(true);
            }

            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("KBNTIND");
            stage.setScene(new Scene(root));

            JFXButton accept = (JFXButton) root.lookup("#acceptPlugins");
            accept.fire();

            FXApp.infoFromTables.asTempateName = "KBNTIND";
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void setCleanPlugins() {
        Parent root;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxmls/Plugins.fxml"));
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Clean");
            stage.setScene(new Scene(root));

            JFXButton accept = (JFXButton) root.lookup("#acceptPlugins");
            accept.fire();

            FXApp.infoFromTables.asTempateName = "Clean";
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void checkConnectAS(ActionEvent actionEvent) {
        new Thread(() -> {
            VMControlWorker vmControlWorker = new VMControlWorker(ASDeployHostBox.getSelectionModel().getSelectedItem(), 7);
            vmControlWorker.start();
            try {
                vmControlWorker.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Platform.runLater(() -> {
                if (vmControlWorker.isOnoffStatus()) {
                    deployASButton.setStyle("-fx-background-color: #a0dd88; -fx-background-radius: 100;");
                } else {
                    deployASButton.setStyle("-fx-background-color: #ee9999; -fx-background-radius: 100;");
                }
            });
            vmControlWorker.interrupt();
        }).start();
    }

    @FXML
    public void checkConnectWeb(ActionEvent actionEvent) {
        new Thread(() -> {
            VMControlWorker vmControlWorker = new VMControlWorker(WebDeployHostBox.getSelectionModel().getSelectedItem(), 7);
            vmControlWorker.start();
            try {
                vmControlWorker.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Platform.runLater(() -> {
                if (vmControlWorker.isOnoffStatus()) {
                    webDeployButton.setStyle("-fx-background-color: #a0dd88; -fx-background-radius: 100;");
                    gfInfoButton.setStyle("-fx-background-color: #a0dd88; -fx-background-radius: 100;");
                    enableGFButton.setStyle("-fx-background-color: #a0dd88; -fx-background-radius: 100;");
                    disableGFButton.setStyle("-fx-background-color: #a0dd88; -fx-background-radius: 100;");
                    ReloadGFButton.setStyle("-fx-background-color: #a0dd88; -fx-background-radius: 100;");
                } else {
                    webDeployButton.setStyle("-fx-background-color: #ee9999; -fx-background-radius: 100;");
                    gfInfoButton.setStyle("-fx-background-color: #ee9999; -fx-background-radius: 100;");
                    enableGFButton.setStyle("-fx-background-color: #ee9999; -fx-background-radius: 100;");
                    disableGFButton.setStyle("-fx-background-color: #ee9999; -fx-background-radius: 100;");
                    ReloadGFButton.setStyle("-fx-background-color: #ee9999; -fx-background-radius: 100;");
                }
            });
            vmControlWorker.interrupt();
        }).start();
    }

    @FXML
    public void checkConnectInfo(ActionEvent actionEvent) {
        new Thread(() -> {
            VMControlWorker vmControlWorker = new VMControlWorker(infoHostBox.getSelectionModel().getSelectedItem(), 7);
            vmControlWorker.start();
            try {
                vmControlWorker.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Platform.runLater(() -> {
                if (vmControlWorker.isOnoffStatus()) {
                    DatabseButton.setStyle("-fx-background-color: #a0dd88; -fx-background-radius: 100;");
                    startGFButton.setStyle("-fx-background-color: #a0dd88; -fx-background-radius: 100;");
                    stopGFButton.setStyle("-fx-background-color: #a0dd88; -fx-background-radius: 100;");
                    restartGFButton.setStyle("-fx-background-color: #a0dd88; -fx-background-radius: 100;");
                    puttyCmdButton.setStyle("-fx-background-color: #a0dd88; -fx-background-radius: 100;");
                    //puttyCmdAllOnline.setStyle("-fx-background-color: #a0dd88; -fx-background-radius: 100;");
                    puttyCmdLogButton.setStyle("-fx-background-color: #a0dd88; -fx-background-radius: 100;");
                    setDateBUtton.setStyle("-fx-background-color: #a0dd88; -fx-background-radius: 100;");
                    GFAppsButton.setStyle("-fx-background-color: #a0dd88; -fx-background-radius: 100;");
                    killJavaButton.setStyle("-fx-background-color: #a0dd88; -fx-background-radius: 100;");
                } else {
                    DatabseButton.setStyle("-fx-background-color: #ee9999; -fx-background-radius: 100;");
                    startGFButton.setStyle("-fx-background-color: #ee9999; -fx-background-radius: 100;");
                    stopGFButton.setStyle("-fx-background-color: #ee9999; -fx-background-radius: 100;");
                    restartGFButton.setStyle("-fx-background-color: #ee9999; -fx-background-radius: 100;");
                    puttyCmdButton.setStyle("-fx-background-color: #ee9999; -fx-background-radius: 100;");
                    //puttyCmdAllOnline.setStyle("-fx-background-color: #ee9999; -fx-background-radius: 100;");
                    puttyCmdLogButton.setStyle("-fx-background-color: #ee9999; -fx-background-radius: 100;");
                    setDateBUtton.setStyle("-fx-background-color: #ee9999; -fx-background-radius: 100;");
                    GFAppsButton.setStyle("-fx-background-color: #ee9999; -fx-background-radius: 100;");
                    killJavaButton.setStyle("-fx-background-color: #ee9999; -fx-background-radius: 100;");
                }
            });
            vmControlWorker.interrupt();
        }).start();
    }

    @FXML
    public void openDataBaseInfo(ActionEvent actionEvent) {
        new Thread(() -> {
            VMControlWorker vmControlWorker = new VMControlWorker(infoHostBox.getSelectionModel().getSelectedItem(), 9);
            vmControlWorker.start();
            try {
                vmControlWorker.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Platform.runLater(() -> {
                JFXDialogLayout content = new JFXDialogLayout();
                Text text = new Text(vmControlWorker.getResult());
                text.setFont(new Font("Arial", 14));
                content.setHeading(new Text(infoHostBox.getSelectionModel().getSelectedItem()));
                content.setBody(text);
                JFXDialog dialog = new JFXDialog(rootStackPane, content, JFXDialog.DialogTransition.CENTER);
                JFXButton button = new JFXButton("close");
                button.setOnAction(event1 -> dialog.close());
                content.setActions(button);
                dialog.toFront();
                dialog.show();
            });
            vmControlWorker.interrupt();
        }).start();
    }

    @FXML
    public void getGfAppsINfo(ActionEvent actionEvent) {
        new Thread(() -> {
            VMControlWorker vmControlWorker = new VMControlWorker(infoHostBox.getSelectionModel().getSelectedItem(), 6);
            vmControlWorker.start();
            try {
                vmControlWorker.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Platform.runLater(() -> {
                JFXDialogLayout content = new JFXDialogLayout();
                Text text = new Text(vmControlWorker.getResult());
                text.setFont(new Font("Arial", 14));
                content.setHeading(new Text(infoHostBox.getSelectionModel().getSelectedItem()));
                content.setBody(text);
                JFXDialog dialog = new JFXDialog(rootStackPane, content, JFXDialog.DialogTransition.CENTER);
                JFXButton button = new JFXButton("close");
                button.setOnAction(event1 -> dialog.close());
                content.setActions(button);
                dialog.toFront();
                dialog.show();
            });
            vmControlWorker.interrupt();
        }).start();
    }

    @FXML
    public void openStartGFButton(ActionEvent actionEvent) {
        new Thread(() -> {
            VMControlWorker vmControlWorker = new VMControlWorker(infoHostBox.getSelectionModel().getSelectedItem(), 0);
            vmControlWorker.start();
            try {
                vmControlWorker.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Platform.runLater(() -> {
                JFXDialogLayout content = new JFXDialogLayout();
                Text text = new Text(vmControlWorker.getResult());
                text.setFont(new Font("Arial", 14));
                content.setHeading(new Text(infoHostBox.getSelectionModel().getSelectedItem()));
                content.setBody(text);
                JFXDialog dialog = new JFXDialog(rootStackPane, content, JFXDialog.DialogTransition.CENTER);
                JFXButton button = new JFXButton("close");
                button.setOnAction(event1 -> dialog.close());
                content.setActions(button);
                dialog.toFront();
                dialog.show();
            });
            vmControlWorker.interrupt();
        }).start();
    }

    @FXML
    public void openStopGFButton(ActionEvent actionEvent) {
        new Thread(() -> {
            VMControlWorker vmControlWorker = new VMControlWorker(infoHostBox.getSelectionModel().getSelectedItem(), 1);
            vmControlWorker.start();
            try {
                vmControlWorker.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Platform.runLater(() -> {
                JFXDialogLayout content = new JFXDialogLayout();
                Text text = new Text(vmControlWorker.getResult());
                text.setFont(new Font("Arial", 14));
                content.setHeading(new Text(infoHostBox.getSelectionModel().getSelectedItem()));
                content.setBody(text);
                JFXDialog dialog = new JFXDialog(rootStackPane, content, JFXDialog.DialogTransition.CENTER);
                JFXButton button = new JFXButton("close");
                button.setOnAction(event1 -> dialog.close());
                content.setActions(button);
                dialog.toFront();
                dialog.show();
            });
            vmControlWorker.interrupt();
        }).start();
    }

    @FXML
    public void openRestartGFButton(ActionEvent actionEvent) {
        new Thread(() -> {
            VMControlWorker vmControlWorker = new VMControlWorker(infoHostBox.getSelectionModel().getSelectedItem(), 2);
            vmControlWorker.start();
            try {
                vmControlWorker.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Platform.runLater(() -> {
                JFXDialogLayout content = new JFXDialogLayout();
                Text text = new Text(vmControlWorker.getResult());
                text.setFont(new Font("Arial", 14));
                content.setHeading(new Text(infoHostBox.getSelectionModel().getSelectedItem()));
                content.setBody(text);
                JFXDialog dialog = new JFXDialog(rootStackPane, content, JFXDialog.DialogTransition.CENTER);
                JFXButton button = new JFXButton("close");
                button.setOnAction(event1 -> dialog.close());
                content.setActions(button);
                dialog.toFront();
                dialog.show();
            });
            vmControlWorker.interrupt();
        }).start();
    }

    @FXML
    public void openPuttyCmd(ActionEvent actionEvent) {
        new Thread(() -> {
            try {
                Process puttyCmd = Runtime.getRuntime().exec(FXApp.puttyPath + " -ssh root@" + infoHostBox.getSelectionModel().getSelectedItem() + " -pw root");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    @FXML
    public void openPuttyCmdAllOnline(ActionEvent actionEvent) {
        new Thread(() -> {
            OnlineChecker onlineChecker = new OnlineChecker();
            onlineChecker.run();
            try {
                onlineChecker.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            PuttyWorker puttyWorker;
            puttyWorker = new PuttyWorker(FXApp.puttyPath, onlineChecker.normalHosts);
            puttyWorker.run();
        }).start();
    }

    @FXML
    public void openPuttyCmdLog(ActionEvent actionEvent) {
        new Thread(() -> {
            File puttyLogCmd = new File(System.getProperty("java.io.tmpdir") + "openPuttyLog.txt");
            new fileUtil().writer(puttyLogCmd, "tail -f -n " + stringInLog.getText() + " /opt/glassfish4/glassfish/domains/storm/logs/server.log");
            try {
                Process puttyCmd = Runtime.getRuntime().exec(FXApp.puttyPath + " -ssh root@" + infoHostBox.getSelectionModel().getSelectedItem() + " -pw root -m " + puttyLogCmd.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    @FXML
    public void openKillJava(ActionEvent actionEvent) {
        new Thread(() -> {
            VMControlWorker vmControlWorker = new VMControlWorker(infoHostBox.getSelectionModel().getSelectedItem(), 4);
            vmControlWorker.start();
            try {
                vmControlWorker.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Platform.runLater(() -> {
                JFXDialogLayout content = new JFXDialogLayout();
                Text text = new Text(vmControlWorker.getResult());
                text.setFont(new Font("Arial", 14));
                content.setHeading(new Text(infoHostBox.getSelectionModel().getSelectedItem()));
                content.setBody(text);
                JFXDialog dialog = new JFXDialog(rootStackPane, content, JFXDialog.DialogTransition.CENTER);
                JFXButton button = new JFXButton("close");
                button.setOnAction(event1 -> dialog.close());
                content.setActions(button);
                dialog.toFront();
                dialog.show();
            });
            vmControlWorker.interrupt();
        }).start();
    }

    @FXML
    public void openSetDate(ActionEvent actionEvent) {
        new Thread(() -> {

            VMControlWorker vmControlWorker = new VMControlWorker(infoHostBox.getSelectionModel().getSelectedItem(), 13);

            vmControlWorker.setBigDate(dateSetter.getValue().getDayOfMonth() + " " + dateSetter.getValue().getMonth() + " " + dateSetter.getValue().getYear()
                    + " " + timeSetter.getValue().getHour() + ":" + timeSetter.getValue().getMinute() + ":" + timeSetter.getValue().getSecond());

            vmControlWorker.start();
            try {
                vmControlWorker.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Platform.runLater(() -> {
                JFXDialogLayout content = new JFXDialogLayout();
                Text text = new Text(vmControlWorker.getResult());
                text.setFont(new Font("Arial", 14));
                content.setHeading(new Text(infoHostBox.getSelectionModel().getSelectedItem()));
                content.setBody(text);
                JFXDialog dialog = new JFXDialog(rootStackPane, content, JFXDialog.DialogTransition.CENTER);
                JFXButton button = new JFXButton("close");
                button.setOnAction(event1 -> dialog.close());
                content.setActions(button);
                dialog.toFront();
                dialog.show();
            });

            vmControlWorker.interrupt();
        }).start();
        logger.info(dateSetter.getValue().getDayOfMonth() + " " + dateSetter.getValue().getMonth() + " " + dateSetter.getValue().getYear()
                + " " + timeSetter.getValue().getHour() + ":" + timeSetter.getValue().getMinute() + ":" + timeSetter.getValue().getSecond());
    }

    @FXML
    public void openAllinfoButton(ActionEvent actionEvent) throws Exception {
        executor = Executors.newFixedThreadPool(new LocalUtils().getHosts().size());
        ArrayList<JFXTextArea> areas = new ArrayList<>();
        for (String host : new LocalUtils().getHosts()) {
            JFXTextArea textInfoArea = new JFXTextArea();
            textInfoArea.setEditable(false);
            textInfoArea.setFont(new Font("Arial", 14));
            areas.add(textInfoArea);
            VMControlWorker vmControlWorker = new VMControlWorker(host, 5, textInfoArea);
            executor.execute(vmControlWorker);
        }
        //executor.shutdown();
        executor.awaitTermination(5000, TimeUnit.MILLISECONDS);
        Platform.runLater(() -> {
            JFXMasonryPane rootMasonry = new JFXMasonryPane();
            for (JFXTextArea area : areas) {
                rootMasonry.getChildren().add(area);
            }

            Scene allInfoScene = new Scene(rootMasonry, 600, 800);
            Stage infoStage = new Stage();

            infoStage.setTitle("All testvms info");
            infoStage.setScene(allInfoScene);
            infoStage.toFront();
            infoStage.show();
        });
    }

    @FXML
    public void openStartGFApp(ActionEvent actionEvent) {
        new Thread(() -> {
            VMControlWorker vmControlWorker = new VMControlWorker(WebDeployHostBox.getSelectionModel().getSelectedItem(), 10);
            vmControlWorker.setWebAppName(getCheckedGFApp());
            vmControlWorker.start();
            try {
                vmControlWorker.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Platform.runLater(() -> {
                JFXDialogLayout content = new JFXDialogLayout();
                Text text = new Text(vmControlWorker.getResult());
                text.setFont(new Font("Arial", 14));
                content.setHeading(new Text(infoHostBox.getSelectionModel().getSelectedItem()));
                content.setBody(text);
                JFXDialog dialog = new JFXDialog(rootStackPane, content, JFXDialog.DialogTransition.CENTER);
                JFXButton button = new JFXButton("close");
                button.setOnAction(event1 -> dialog.close());
                content.setActions(button);
                dialog.toFront();
                dialog.show();
            });
            vmControlWorker.interrupt();
        }).start();
    }

    @FXML
    public void openStopGFApp(ActionEvent actionEvent) {
        new Thread(() -> {
            VMControlWorker vmControlWorker = new VMControlWorker(WebDeployHostBox.getSelectionModel().getSelectedItem(), 11);
            vmControlWorker.setWebAppName(getCheckedGFApp());
            vmControlWorker.start();
            try {
                vmControlWorker.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Platform.runLater(() -> {
                JFXDialogLayout content = new JFXDialogLayout();
                Text text = new Text(vmControlWorker.getResult());
                text.setFont(new Font("Arial", 14));
                content.setHeading(new Text(infoHostBox.getSelectionModel().getSelectedItem()));
                content.setBody(text);
                JFXDialog dialog = new JFXDialog(rootStackPane, content, JFXDialog.DialogTransition.CENTER);
                JFXButton button = new JFXButton("close");
                button.setOnAction(event1 -> dialog.close());
                content.setActions(button);
                dialog.toFront();
                dialog.show();
            });
            vmControlWorker.interrupt();
        }).start();
    }

    public void openReloadGFApp(ActionEvent actionEvent) {
        new Thread(() -> {
            VMControlWorker vmControlWorker = new VMControlWorker(WebDeployHostBox.getSelectionModel().getSelectedItem(), 12);
            vmControlWorker.setWebAppName(getCheckedGFApp());
            vmControlWorker.start();
            try {
                vmControlWorker.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Platform.runLater(() -> {
                JFXDialogLayout content = new JFXDialogLayout();
                Text text = new Text(vmControlWorker.getResult());
                text.setFont(new Font("Arial", 14));
                content.setHeading(new Text(infoHostBox.getSelectionModel().getSelectedItem()));
                content.setBody(text);
                JFXDialog dialog = new JFXDialog(rootStackPane, content, JFXDialog.DialogTransition.CENTER);
                JFXButton button = new JFXButton("close");
                button.setOnAction(event1 -> dialog.close());
                content.setActions(button);
                dialog.toFront();
                dialog.show();
            });
            vmControlWorker.interrupt();
        }).start();
    }

    private String getCheckedGFApp() {
        JFXRadioButton selectedGFApp = (JFXRadioButton) gfAppGroup.getSelectedToggle();
        if ((selectedGFApp.getText()).equals("  ")) {
            return yourGFApp.getText();
        } else {
            return selectedGFApp.getText();
        }
    }

    private String getWebTemplate() {
        JFXRadioButton selectedWebTemplate = (JFXRadioButton) webTemplageGroup.getSelectedToggle();
        if ((selectedWebTemplate.getText()).equals(" ")) {
            return yourWebTempalte.getText();
        } else {
            return selectedWebTemplate.getText();
        }
    }

    @FXML
    public void openArchiveTab(Event event) {
        if (archiveTab.isSelected() && !asFileChosen) {
            asfileDetector();
        }
    }

    @FXML
    public void openWebTab(Event event) {
        if (webTab.isSelected() && !webFileChosen) {
            webFileDetector();
        }
    }

    @FXML
    public void openASFile(ActionEvent actionEvent) {
        try {
            asFileChosen = true;
            File file = fileChooser.showOpenDialog(null);
            if (file != null && file.getName().contains("Stor-M_3")) {
                asFileToDeploy = file;
                asFileChosen = true;
                labelForFileASDeploy.setText(file.getName());
                labelForFileASDeploy.setStyle("-fx-background-color: #a0dd88");
                logger.info("Chosen file " + file.getName());
            } else {
                asFileChosen = false;
                logger.warn("File broken or havn`t Stor-M_3 in namespace. Back to autodetect");
            }
        } catch (Exception e) {
            asFileChosen = false;
            logger.error(e);
        }

    }

    @FXML
    public void openWebFile(ActionEvent actionEvent) {
        try {
            webFileChosen = true;
            File file = fileChooser.showOpenDialog(null);
            if (file != null && file.getName().contains("Stor-M_Web")) {
                webFileToDeploy = file;
                webFileChosen = true;
                labelForFileWebDeploy.setText(file.getName());
                labelForFileWebDeploy.setStyle("-fx-background-color: #a0dd88");
                logger.info("Chosen file " + file.getName());
            } else {
                webFileChosen = false;
                logger.warn("File broken or havn`t Stor-M_Web in namespace. Back to autodetect");
            }
        } catch (Exception e) {
            webFileChosen = false;
            logger.error(e);
        }
    }

    @FXML
    public void refreshASFile(ActionEvent actionEvent) {
        asFileChosen = false;
        asfileDetector();
    }

    @FXML
    public void refreshWebFile(ActionEvent actionEvent) {
        webFileChosen = false;
        webFileDetector();
    }

    private void asfileDetector() {
        fileUtil fileUtil = new fileUtil();
        File downloadedZip = fileUtil.getMostRecentFile(Paths.get(System.getProperty("user.home") + "\\Downloads\\"), "Stor-M_3");
        if (downloadedZip == null) {
            labelForFileASDeploy.setText("No file");
            labelForFileASDeploy.setStyle("-fx-background-color: #ee9999");
            logger.warn("Cant autodetect file for AS ");
        } else {
            labelForFileASDeploy.setText(downloadedZip.getName());
            labelForFileASDeploy.setStyle("-fx-background-color: #a0dd88");
            logger.info("Detected file for AS " + downloadedZip.getName());
        }
    }

    private void webFileDetector() {
        fileUtil fileUtil = new fileUtil();
        File downloadedZip = fileUtil.getMostRecentFile(Paths.get(System.getProperty("user.home") + "\\Downloads\\"), "Stor-M_Web");
        if (downloadedZip == null) {
            labelForFileWebDeploy.setText("No file");
            labelForFileWebDeploy.setStyle("-fx-background-color: #ee9999");
            logger.warn("Cant autodetect file for WEB ");
        } else {
            labelForFileWebDeploy.setText(downloadedZip.getName());
            labelForFileWebDeploy.setStyle("-fx-background-color: #a0dd88");
            logger.info("Detected file for WEB " + downloadedZip.getName());
        }
    }

    public void catchDraggedFile(DragEvent dragEvent) {
        if (dragEvent.getDragboard().hasFiles()) {
            dragEvent.acceptTransferModes(TransferMode.ANY);
        }
    }

    public void processDraggedFile(DragEvent dragEvent) {
        File candidateFile = dragEvent.getDragboard().getFiles().get(0);
        if (!webTab.isSelected() && !archiveTab.isSelected() && !massImportTab.isSelected()) {
            logger.warn("Wrong tab opened. Select deploy tab to DND");
        } else {
            if (webTab.isSelected() && candidateFile.getName().contains("Stor-M_Web")) {
                webFileToDeploy = candidateFile;
                webFileChosen = true;
                labelForFileWebDeploy.setText(candidateFile.getName());
                labelForFileWebDeploy.setStyle("-fx-background-color: #a0dd88");
                logger.info("Detected file for WEB " + candidateFile.getName());
                dragEvent.consume();
            } else {
                if (archiveTab.isSelected() && candidateFile.getName().contains("Stor-M_3")) {
                    asFileToDeploy = candidateFile;
                    asFileChosen = true;
                    labelForFileASDeploy.setText(candidateFile.getName());
                    labelForFileASDeploy.setStyle("-fx-background-color: #a0dd88");
                    logger.info("Detected file for AS " + candidateFile.getName());
                    dragEvent.consume();
                } else {
                    if (massImportTab.isSelected()) {
                        analyzeFiletoMassDeploy(candidateFile);
                        toMassDeploy = candidateFile;
                        mdFileChosen = true;
                        fileToMassDeploy.setText(toMassDeploy.getName());
                    }
                }
            }
        }
    }

    private void analyzeFiletoMassDeploy(File file) {
        if (file.getName().contains("Web")) {
            ArrayList<String> webConfig = new ArrayList<>();
            webConfig.add("Default");
            webConfig.add("EBSTouch");
            webConfig.add("EBS");
            webConfig.add("MCHSTouch");
            webConfig.add("MCHS");
            webConfig.add("RussianHelicopters");
            webConfig.add("OMK");
            webConfig.add("KBNTI");
            webConfig.add("KSMK");
            webConfig.add("NormdocsCatalog");
            webConfig.add("KBNTI2");
            webConfig.add("KBNTIND");

            webToggle.selectedProperty().setValue(true);
            testvm6Configure.setItems(FXCollections.observableList(webConfig));
            testvm12Configure.setItems(FXCollections.observableList(webConfig));

            testvmConfigure.setText("Default");
            testvm3Configure.setText("Ebs + touch");
            testvm10Configure.setText("RussianHelicopters");
            testvm11Configure.setText("MCHS + touch");
            testvm13Configure.setText("KBNTI2");
            testvm14Configure.setText("KBNTI2 + KSMK");
        } else {
            ArrayList<String> asConfig = new ArrayList<>();
            asConfig.add("Default");
            asConfig.add("Ebs");
            asConfig.add("Mchs");
            asConfig.add("Helicopters");
            asConfig.add("OMK");
            asConfig.add("CQMS");
            asConfig.add("KBNTI");
            asConfig.add("Clean");

            asToggle.selectedProperty().setValue(true);
            testvm6Configure.setItems(FXCollections.observableList(asConfig));
            testvm12Configure.setItems(FXCollections.observableList(asConfig));

            testvmConfigure.setText("Default");
            testvm3Configure.setText("Ebs");
            testvm10Configure.setText("Helicopters");
            testvm11Configure.setText("Mchs");
            testvm13Configure.setText("OMK");
            testvm14Configure.setText("CQMS");
        }
    }

    public void openFileToMassDeploy(ActionEvent actionEvent) {
        try {
            toMassDeploy = fileChooser.showOpenDialog(null);
            analyzeFiletoMassDeploy(toMassDeploy);
            fileToMassDeploy.setText(toMassDeploy.getName());
        } catch (Exception e) {
            logger.error(e);
        }
    }

    public void startMassDeploy(ActionEvent actionEvent) {
        ExecutorService executorService = Executors.newFixedThreadPool(20);

        if (testvmRadioButton.isSelected()) {
            executorService.submit(new Thread(() -> {
                try {
                    FileUtils.copyFile(toMassDeploy, new File(System.getProperty("user.home") + "\\vmupdater\\" + toMassDeploy.getName() + "toDefaultMassDeploy.zip"));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                File toCurrentDeploy = new File(System.getProperty("user.home") + "\\vmupdater\\" + toMassDeploy.getName() + "toDefaultMassDeploy.zip");

                InfoFromTables infoFromTables = new InfoFromTables();
                if (toCurrentDeploy.getName().contains("Web")) {
                    infoFromTables.webTemplateName = testvmConfigure.getText();
                    infoFromTables.hostname = "testvm";
                    infoFromTables.glassfishAppName = "StormWebApplication";

                    WebWorker webWorker = new WebWorker(infoFromTables);
                    webWorker.setFileToDeploy(toCurrentDeploy);
                    webWorker.start();
                } else {
                    infoFromTables.clientPlugins = shittySettings.ebsClient;
                    infoFromTables.serverPlugins = shittySettings.ebsServer;
                    infoFromTables.hostname = "testvm";
                    infoFromTables.glassfishAppName = "ArchiveServer";
                    infoFromTables.asTempateName = "Default";
                    infoFromTables.synchroProxy = false;

                    ASWorker asWorker = new ASWorker(infoFromTables);
                    asWorker.setFileToDeploy(toCurrentDeploy);
                    asWorker.start();
                }
            }));
        }

        if (testvm3RadioButton.isSelected()) {
            executorService.submit(new Thread(() -> {
                logger.info("Copying temp zip");
                try {
                    FileUtils.copyFile(toMassDeploy, new File(System.getProperty("user.home") + "\\vmupdater\\" + toMassDeploy.getName() + "toEBSMassDeploy.zip"));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                File toCurrentDeploy = new File(System.getProperty("user.home") + "\\vmupdater\\" + toMassDeploy.getName() + "toEBSMassDeploy.zip");

                logger.info("Start");
                InfoFromTables infoFromTables = new InfoFromTables();
                if (toMassDeploy.getName().contains("Web")) {
                    infoFromTables.webTemplateName = "EBSTouch";
                    infoFromTables.hostname = "testvm3";
                    infoFromTables.glassfishAppName = "StormWebApplicationTouch";

                    WebWorker webWorker = new WebWorker(infoFromTables);
                    webWorker.setFileToDeploy(toCurrentDeploy);
                    webWorker.run();

                    InfoFromTables infoFromTables2 = new InfoFromTables();
                    infoFromTables2.webTemplateName = "EBS";
                    infoFromTables2.hostname = "testvm3";
                    infoFromTables2.glassfishAppName = "StormWebApplication";

                    WebWorker webWorkertouch = new WebWorker(infoFromTables2);
                    webWorkertouch.setFileToDeploy(toCurrentDeploy);
                    webWorkertouch.start();
                } else {
                    infoFromTables.clientPlugins = shittySettings.ebsClient;
                    infoFromTables.serverPlugins = shittySettings.ebsServer;
                    infoFromTables.hostname = "testvm3";
                    infoFromTables.glassfishAppName = "ArchiveServer";
                    infoFromTables.asTempateName = "Ebs";
                    infoFromTables.synchroProxy = false;

                    ASWorker asWorker = new ASWorker(infoFromTables);
                    asWorker.setFileToDeploy(toCurrentDeploy);
                    asWorker.start();
                }
            }));
        }

        if (testvm6RadioButton.isSelected()) {
            executorService.submit(new Thread(() -> {
                try {
                    FileUtils.copyFile(toMassDeploy, new File(System.getProperty("user.home") + "\\vmupdater\\" + toMassDeploy.getName() + "toTestvm6MassDeploy.zip"));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                File toCurrentDeploy = new File(System.getProperty("user.home") + "\\vmupdater\\" + toMassDeploy.getName() + "toTestvm6MassDeploy.zip");

                InfoFromTables infoFromTables = new InfoFromTables();
                if (toMassDeploy.getName().contains("Web")) {
                    infoFromTables.webTemplateName = testvm6Configure.getSelectionModel().getSelectedItem();
                    infoFromTables.hostname = "testvm6";
                    infoFromTables.glassfishAppName = "StormWebApplication";

                    WebWorker webWorker = new WebWorker(infoFromTables);
                    webWorker.setFileToDeploy(toCurrentDeploy);
                    webWorker.start();
                } else {
                    String selected = testvm6Configure.getSelectionModel().getSelectedItem();
                    String serverTemp = "";
                    String clientTemp = "";

                    if (selected.equals("Default")) {
                        serverTemp = shittySettings.defaultServer;
                        clientTemp = shittySettings.defaultClient;
                    }
                    if (selected.equals("Ebs")) {
                        serverTemp = shittySettings.ebsServer;
                        clientTemp = shittySettings.ebsClient;
                    }
                    if (selected.equals("Mchs")) {
                        serverTemp = shittySettings.mchsServer;
                        clientTemp = shittySettings.mchsClient;
                    }
                    if (selected.equals("Helicopters")) {
                        serverTemp = shittySettings.heliServer;
                        clientTemp = shittySettings.heliClient;
                    }
                    if (selected.equals("OMK")) {
                        serverTemp = shittySettings.omkServer;
                        clientTemp = shittySettings.omkClient;
                    }
                    if (selected.equals("CQMS")) {
                        serverTemp = shittySettings.cqmsServer;
                        clientTemp = shittySettings.cqmsClient;
                    }
                    if (selected.equals("KBNTI")) {
                        serverTemp = shittySettings.kbntiServer;
                        clientTemp = shittySettings.kbntiClient;
                    }
                    if (selected.equals("Clean")) {
                        serverTemp = shittySettings.cleanServer;
                        clientTemp = shittySettings.cleanClient;
                    }

                    infoFromTables.clientPlugins = clientTemp;
                    infoFromTables.serverPlugins = serverTemp;
                    infoFromTables.hostname = "testvm6";
                    infoFromTables.glassfishAppName = "ArchiveServer";
                    infoFromTables.asTempateName = testvm6Configure.getSelectionModel().getSelectedItem();
                    infoFromTables.synchroProxy = false;

                    ASWorker asWorker = new ASWorker(infoFromTables);
                    asWorker.setFileToDeploy(toCurrentDeploy);
                    asWorker.start();
                }
            }));
        }

        if (testvm10RadioButton.isSelected()) {
            executorService.submit(new Thread(() -> {
                try {
                    FileUtils.copyFile(toMassDeploy, new File(System.getProperty("user.home") + "\\vmupdater\\" + toMassDeploy.getName() + "toHeliMassDeploy.zip"));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                File toCurrentDeploy = new File(System.getProperty("user.home") + "\\vmupdater\\" + toMassDeploy.getName() + "toHeliMassDeploy.zip");

                InfoFromTables infoFromTables = new InfoFromTables();
                if (toMassDeploy.getName().contains("Web")) {
                    infoFromTables.webTemplateName = testvm10Configure.getText();
                    infoFromTables.hostname = "testvm10";
                    infoFromTables.glassfishAppName = "StormWebApplication";

                    WebWorker webWorker = new WebWorker(infoFromTables);
                    webWorker.setFileToDeploy(toCurrentDeploy);
                    webWorker.start();
                } else {
                    infoFromTables.clientPlugins = shittySettings.heliClient;
                    infoFromTables.serverPlugins = shittySettings.heliServer;
                    infoFromTables.hostname = "testvm10";
                    infoFromTables.glassfishAppName = "ArchiveServer";
                    infoFromTables.asTempateName = "Helicopters";
                    infoFromTables.synchroProxy = false;

                    ASWorker asWorker = new ASWorker(infoFromTables);
                    asWorker.setFileToDeploy(toCurrentDeploy);
                    asWorker.start();
                }
            }));
        }

        if (testvm11RadioButton.isSelected()) {
            executorService.submit(new Thread(() -> {
                try {
                    FileUtils.copyFile(toMassDeploy, new File(System.getProperty("user.home") + "\\vmupdater\\" + toMassDeploy.getName() + "toMCHSMassDeploy.zip"));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                File toCurrentDeploy = new File(System.getProperty("user.home") + "\\vmupdater\\" + toMassDeploy.getName() + "toMCHSMassDeploy.zip");

                InfoFromTables infoFromTables;
                if (toMassDeploy.getName().contains("Web")) {
                    infoFromTables = new InfoFromTables();
                    infoFromTables.webTemplateName = "MCHS";
                    infoFromTables.hostname = "testvm11";
                    infoFromTables.glassfishAppName = "StormWebApplication";

                    WebWorker webWorker = new WebWorker(infoFromTables);
                    webWorker.setFileToDeploy(toCurrentDeploy);
                    webWorker.run();

                    InfoFromTables infoFromTables2 = new InfoFromTables();
                    infoFromTables2.webTemplateName = "MCHSTouch";
                    infoFromTables2.hostname = "testvm11";
                    infoFromTables2.glassfishAppName = "StormWebApplicationTouch";

                    WebWorker webWorker2 = new WebWorker(infoFromTables2);
                    webWorker2.setFileToDeploy(toCurrentDeploy);
                    webWorker2.run();
                } else {
                    infoFromTables = new InfoFromTables();
                    infoFromTables.clientPlugins = shittySettings.mchsClient;
                    infoFromTables.serverPlugins = shittySettings.mchsServer;
                    infoFromTables.hostname = "testvm11";
                    infoFromTables.glassfishAppName = "ArchiveServer";
                    infoFromTables.asTempateName = "Mchs";
                    infoFromTables.synchroProxy = true;

                    ASWorker asWorker = new ASWorker(infoFromTables);
                    asWorker.setFileToDeploy(toCurrentDeploy);
                    asWorker.run();
                }
            }));
        }

        if (testvm12RadioButton.isSelected()) {
            executorService.submit(new Thread(() -> {
                try {
                    FileUtils.copyFile(toMassDeploy, new File(System.getProperty("user.home") + "\\vmupdater\\" + toMassDeploy.getName() + "toTestvm12MassDeploy.zip"));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                File toCurrentDeploy = new File(System.getProperty("user.home") + "\\vmupdater\\" + toMassDeploy.getName() + "toTestvm12MassDeploy.zip");

                InfoFromTables infoFromTables = new InfoFromTables();
                if (toMassDeploy.getName().contains("Web")) {
                    infoFromTables.webTemplateName = testvm12Configure.getSelectionModel().getSelectedItem();
                    infoFromTables.hostname = "testvm12";
                    infoFromTables.glassfishAppName = "StormWebApplication";

                    WebWorker webWorker = new WebWorker(infoFromTables);
                    webWorker.setFileToDeploy(toCurrentDeploy);
                    webWorker.start();
                } else {
                    String selected = testvm6Configure.getSelectionModel().getSelectedItem();
                    String serverTemp = "";
                    String clientTemp = "";

                    if (selected.equals("Default")) {
                        serverTemp = shittySettings.defaultServer;
                        clientTemp = shittySettings.defaultClient;
                    }
                    if (selected.equals("Ebs")) {
                        serverTemp = shittySettings.ebsServer;
                        clientTemp = shittySettings.ebsClient;
                    }
                    if (selected.equals("Mchs")) {
                        serverTemp = shittySettings.mchsServer;
                        clientTemp = shittySettings.mchsClient;
                    }
                    if (selected.equals("Helicopters")) {
                        serverTemp = shittySettings.heliServer;
                        clientTemp = shittySettings.heliClient;
                    }
                    if (selected.equals("OMK")) {
                        serverTemp = shittySettings.omkServer;
                        clientTemp = shittySettings.omkClient;
                    }
                    if (selected.equals("CQMS")) {
                        serverTemp = shittySettings.cqmsServer;
                        clientTemp = shittySettings.cqmsClient;
                    }
                    if (selected.equals("KBNTI")) {
                        serverTemp = shittySettings.kbntiServer;
                        clientTemp = shittySettings.kbntiClient;
                    }
                    if (selected.equals("Clean")) {
                        serverTemp = shittySettings.cleanServer;
                        clientTemp = shittySettings.cleanClient;
                    }

                    infoFromTables.clientPlugins = clientTemp;
                    infoFromTables.serverPlugins = serverTemp;
                    infoFromTables.hostname = "testvm12";
                    infoFromTables.glassfishAppName = "ArchiveServer";
                    infoFromTables.asTempateName = testvm12Configure.getSelectionModel().getSelectedItem();
                    infoFromTables.synchroProxy = false;

                    ASWorker asWorker = new ASWorker(infoFromTables);
                    asWorker.setFileToDeploy(toCurrentDeploy);
                    asWorker.start();
                }
            }));
        }

        if (testvm13RadioButton.isSelected()) {
            executorService.submit(new Thread(() -> {
                try {
                    FileUtils.copyFile(toMassDeploy, new File(System.getProperty("user.home") + "\\vmupdater\\" + toMassDeploy.getName() + "toOMKMassDeploy.zip"));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                File toCurrentDeploy = new File(System.getProperty("user.home") + "\\vmupdater\\" + toMassDeploy.getName() + "toOMKMassDeploy.zip");

                InfoFromTables infoFromTables = new InfoFromTables();
                if (toMassDeploy.getName().contains("Web")) {
                    infoFromTables.webTemplateName = testvm13Configure.getText();
                    infoFromTables.hostname = "testvm13";
                    infoFromTables.glassfishAppName = "StormWebApplication";

                    WebWorker webWorker = new WebWorker(infoFromTables);
                    webWorker.setFileToDeploy(toCurrentDeploy);
                    webWorker.start();
                } else {
                    infoFromTables.clientPlugins = shittySettings.omkClient;
                    infoFromTables.serverPlugins = shittySettings.omkServer;
                    infoFromTables.hostname = "testvm13";
                    infoFromTables.glassfishAppName = "ArchiveServer";
                    infoFromTables.asTempateName = "Omk";
                    infoFromTables.synchroProxy = false;

                    ASWorker asWorker = new ASWorker(infoFromTables);
                    asWorker.setFileToDeploy(toCurrentDeploy);
                    asWorker.start();
                }
            }));
        }

        if (testvm14RadioButton.isSelected()) {
            executorService.submit(new Thread(() -> {
                try {
                    FileUtils.copyFile(toMassDeploy, new File(System.getProperty("user.home") + "\\vmupdater\\" + toMassDeploy.getName() + "toCQMSMassDeploy.zip"));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                File toCurrentDeploy = new File(System.getProperty("user.home") + "\\vmupdater\\" + toMassDeploy.getName() + "toCQMSMassDeploy.zip");

                InfoFromTables infoFromTables = new InfoFromTables();
                if (toMassDeploy.getName().contains("Web")) {
                    infoFromTables.webTemplateName = "KBNTI2";
                    infoFromTables.hostname = "testvm14";
                    infoFromTables.glassfishAppName = "StormWebApplication";

                    WebWorker webWorker = new WebWorker(infoFromTables);
                    webWorker.setFileToDeploy(toCurrentDeploy);
                    webWorker.run();

                    InfoFromTables infoFromTables2 = new InfoFromTables();
                    infoFromTables2.webTemplateName = "KSMK";
                    infoFromTables2.hostname = "testvm14";
                    infoFromTables2.glassfishAppName = "KSMK";

                    WebWorker webWorker2 = new WebWorker(infoFromTables2);
                    webWorker2.setFileToDeploy(toCurrentDeploy);
                    webWorker2.start();
                } else {
                    infoFromTables.clientPlugins = shittySettings.cqmsClient;
                    infoFromTables.serverPlugins = shittySettings.cqmsServer;
                    infoFromTables.hostname = "testvm14";
                    infoFromTables.glassfishAppName = "ArchiveServer";
                    infoFromTables.asTempateName = "CQMS";
                    infoFromTables.synchroProxy = false;

                    ASWorker asWorker = new ASWorker(infoFromTables);
                    asWorker.setFileToDeploy(toCurrentDeploy);
                    asWorker.start();
                }
            }));
        }
    }

    public void openMassImportTab(Event event) {
        if (massImportTab.isSelected() && asToggle.selectedProperty().getValue() && !mdFileChosen) {
            mdFileDetector(true);
        }
        if (massImportTab.isSelected() && webToggle.selectedProperty().getValue() && !mdFileChosen) {
            mdFileDetector(false);
        }
    }

    private void mdFileDetector(boolean isAsToggled) {
        fileUtil fileUtil = new fileUtil();
        File downloadedZip;
        if (isAsToggled) {
            downloadedZip = fileUtil.getMostRecentFile(Paths.get(System.getProperty("user.home") + "\\Downloads\\"), "Stor-M_3");
        } else {
            downloadedZip = fileUtil.getMostRecentFile(Paths.get(System.getProperty("user.home") + "\\Downloads\\"), "Stor-M_Web");
        }
        if (downloadedZip == null) {
            fileToMassDeploy.setText("Not found Archive");
        } else {
            toMassDeploy = downloadedZip;
            fileToMassDeploy.setText(toMassDeploy.getName());
            analyzeFiletoMassDeploy(toMassDeploy);
        }
    }

    public void refreshMDFile(ActionEvent actionEvent) {
        mdFileChosen = false;
        if (massImportTab.isSelected() && asToggle.selectedProperty().getValue() && !mdFileChosen) {
            mdFileDetector(true);
        }
        if (massImportTab.isSelected() && webToggle.selectedProperty().getValue() && !mdFileChosen) {
            mdFileDetector(false);
        }
    }

    public void asMdToggled(ActionEvent actionEvent) {
        if (!mdFileChosen)
            mdFileDetector(true);
    }

    public void webMdToggled(ActionEvent actionEvent) {
        if (!mdFileChosen)
            mdFileDetector(false);
    }
}