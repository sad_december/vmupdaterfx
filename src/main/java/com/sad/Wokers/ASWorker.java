package com.sad.Wokers;

import com.jcraft.jsch.JSchException;
import com.sad.utils.FileTransfer;
import com.sad.utils.InfoFromTables;
import com.sad.utils.fileUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.File;
import java.nio.file.Paths;

public class ASWorker extends Thread {

    private static final Logger logger = LogManager.getLogger(ASWorker.class);

    private String downloadFolder;
    private String templateName;
    private String rootFolder1;
    private String hostName;
    private FileTransfer fileTransfer;
    private InfoFromTables info;
    private String buildVersion = "";
    private boolean featureTag = false;
    private File fileToDeploy;
    private boolean useChosenFIle = false;

    public ASWorker(InfoFromTables infoFromTables) {
        this.downloadFolder = System.getProperty("user.home") + "\\Downloads\\";
        this.templateName = infoFromTables.asTempateName;
        this.info = infoFromTables;
        this.hostName = infoFromTables.hostname;
    }

    @Override
    public void run() {
        unzipDownloaded();
        moveLocal();
        cleanViaTemplate();

        try {
            transfer();
            redeployExecute();
        } catch (JSchException e) {
            e.printStackTrace();
        }
    }

    private void redeployExecute() throws JSchException {
        if (this.info.synchroProxy) {
            fileTransfer.executeCommand(fileTransfer.executeCommand("/opt/glassfish4/glassfish/bin/asadmin --user admin --passwordfile /home/zForUpdateServer/password.txt deploy --force=true --name SynchronizationProxy /home/zForUpdateServer/SynchronizationProxy.war; rm -f file /home/zForUpdateServer/SynchronizationProxy.war"));
        }
        fileTransfer.executeCommand(fileTransfer.executeCommand("/opt/glassfish4/glassfish/bin/asadmin --user admin --passwordfile /home/zForUpdateServer/password.txt deploy --force=true --name ArchiveServer /home/zForUpdateServer/ArchiveServer.war; rm -f file /home/zForUpdateServer/ArchiveServer.war"));

        fileTransfer.closeConnect();
        logger.info("Process Completed");
    }

    private void transfer() throws JSchException {
        fileTransfer = new FileTransfer();
        fileTransfer.sftpConnection(hostName);
        fileTransfer.executeCommand("rm -rf /home/storm/.alee/AleeArchiveServer3/plugins/*");
        fileTransfer.sendFileToServer(rootFolder1 + "ServerPlugins\\*.jar", "/home/storm/.alee/AleeArchiveServer3/plugins/");
        fileTransfer.sendFileToServer(rootFolder1 + "updates.zip", "/home/storm/.alee/AleeArchiveServer3/update/updates.zip");
        fileTransfer.sendFileToServer(rootFolder1 + "ArchiveServer.war", "/home/zForUpdateServer/ArchiveServer.war");
        fileTransfer.sendFileToServer(rootFolder1 + "asVersion.txt", "/home/zForUpdateServer/asVersion.txt");
        if (templateName.equals("Mchs")) {
            fileTransfer.sendFileToServer(rootFolder1 + "SynchronizationProxy.war", "/home/zForUpdateServer/SynchronizationProxy.war");
        }
    }

    private void moveLocal() {
        fileUtil fileUtil = new fileUtil();
        fileUtil.copyFolder(rootFolder1 + "Files\\plugins\\client\\", rootFolder1 + "plugins\\");
        fileUtil.copyFolder(rootFolder1 + "Files\\plugins\\server\\", rootFolder1 + "ServerPlugins\\");
        fileUtil.copyFile(rootFolder1 + "Files\\installers\\updates.zip", rootFolder1 + "updates.zip");
        fileUtil.copyFile(rootFolder1 + "Files\\modules\\ArchiveServer.war", rootFolder1 + "ArchiveServer.war");
        fileUtil.copyFile(rootFolder1 + "Files\\modules\\SynchronizationProxy.war", rootFolder1 + "SynchronizationProxy.war");

        File asVersion = new File(rootFolder1 + "asVersion.txt");
        if (this.featureTag) {
            fileUtil.writer(asVersion, templateName + " feature " + buildVersion + "\n");
        } else {
            fileUtil.writer(asVersion, templateName + " " + buildVersion + "\n");
        }

        fileUtil.deleteDirectory(new File(rootFolder1 + "Files\\"));
    }

    private void cleanViaTemplate() {
        fileUtil fileUtil = new fileUtil();
        fileUtil.deleteFiles(rootFolder1 + "ServerPlugins\\", info.serverPlugins);
        fileUtil.deleteFiles(rootFolder1 + "plugins\\", info.clientPlugins);
        fileUtil.addToZip(rootFolder1 + "updates.zip", rootFolder1 + "plugins\\");
    }

    private void unzipDownloaded() {
        File downloadedZip;
        fileUtil fileUtil = new fileUtil();

        if (!useChosenFIle) {
            downloadedZip = fileUtil.getMostRecentFile(Paths.get(downloadFolder), "Stor-M_3");
        } else {
            downloadedZip = fileToDeploy;
        }
        if (downloadedZip.getName().contains("Feature")) {
            buildVersion = downloadedZip.getName().substring(17, 21);
            this.featureTag = true;
        } else {
            buildVersion = downloadedZip.getName().substring(9, 13);
        }
        String rootFolder = System.getProperty("user.home") + "\\vmUpdater\\" + templateName + "\\" + buildVersion + "\\";
        rootFolder1 = rootFolder;
        try {
            new fileUtil().deleteDirectory(new File(rootFolder1));
            logger.info("Old files in temp deleted");
        } catch (Exception ignored) {
        }
        fileUtil.createFolder(rootFolder);
        fileUtil.createFolder(rootFolder + "plugins\\");
        fileUtil.createFolder(rootFolder + "ServerPlugins\\");
        fileUtil.unzip(downloadedZip.getAbsolutePath(), rootFolder);
    }

    public void setFileToDeploy(File asFileToDeploy) {
        this.fileToDeploy = asFileToDeploy;
        this.useChosenFIle = true;
    }

}
