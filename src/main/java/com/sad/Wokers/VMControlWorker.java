package com.sad.Wokers;

import com.jcraft.jsch.JSchException;
import com.jfoenix.controls.JFXTextArea;
import com.sad.utils.ApiConnector;
import com.sad.utils.FileTransfer;
import com.sad.utils.LocalUtils;
import com.vdurmont.emoji.EmojiParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Callable;


public class VMControlWorker extends Thread implements Callable<String> {

    private ArrayList<String> sadEmoji;
    private static final Logger logger = LogManager.getLogger(ASWorker.class);
    private String hostName;
    private FileTransfer fileTransfer;
    private int iDoperation;
    private String result;
    private String bigDate;
    private String smallDate;
    private int strings;
    private String webAppName;
    private boolean onoffStatus;
    private JFXTextArea textArea;

    public VMControlWorker(String host, int iDoperation, JFXTextArea textInfoArea) {
        this.textArea = textInfoArea;
        this.sadEmoji = new LocalUtils().getSadEmoji();
        this.iDoperation = iDoperation;
        this.hostName = host;
    }

    public void setBigDate(String bigDate) {
        this.bigDate = bigDate;
    }

    public void setSmallDate(String smallDate) {
        this.smallDate = smallDate;
    }

    public String getBigDate() {
        return bigDate;
    }

    public String getSmallDate() {
        return smallDate;
    }

    public void setStrings(int strings) {
        this.strings = strings;
    }

    public void setWebAppName(String webAppName) {
        this.webAppName = webAppName;
    }

    public boolean isOnoffStatus() {
        return onoffStatus;
    }

    public VMControlWorker(String hostName, int iDoperation) {
        this.sadEmoji = new LocalUtils().getSadEmoji();
        this.iDoperation = iDoperation;
        this.hostName = hostName;
    }

    public void closeConnection() {
        try {
            fileTransfer.closeConnect();
            onoffStatus = true;
        } catch (Exception ignored) {
            onoffStatus = false;
        }
    }

    private String getGfApps() {
        StringBuilder sb = new StringBuilder();
        fileTransfer = new FileTransfer();
        fileTransfer.sftpConnection(hostName);
        try {
            fileTransfer.checkConnection();
            sb.append(fileTransfer.getGlassfishApps());
            sb = new StringBuilder(sb.substring(0, sb.indexOf("Command")));
        } catch (JSchException ignored) {
            sb.append("Server offline ").append(EmojiParser.parseToUnicode(sadEmoji.get(new Random().nextInt(sadEmoji.size()))));
        }
        return sb.toString();
    }

    private String executeGFCmd(String cmd) {
        StringBuilder sb = new StringBuilder();
        fileTransfer = new FileTransfer();
        fileTransfer.sftpConnection(hostName);
        try {
            fileTransfer.checkConnection();
            sb.append(fileTransfer.executeCommand(cmd));
            sb.append("\n");
        } catch (JSchException e) {
            sb.append("Server offline ").append(EmojiParser.parseToUnicode(sadEmoji.get(new Random().nextInt(sadEmoji.size()))));
            sb.append("\n");
        }
        return sb.toString();
    }

    private String getVersions() {
        StringBuilder sb = new StringBuilder();
        fileTransfer = new FileTransfer();
        fileTransfer.sftpConnection(hostName);
        try {
            fileTransfer.checkConnection();
            sb.append("AS :")
                    .append(fileTransfer.getInfoFormFile("/home/zForUpdateServer/asVersion.txt"))
                    .append("WEB :")
                    .append(fileTransfer.getInfoFormFile("/home/zForUpdateServer/webVersion.txt"));
        } catch (JSchException e) {
            sb.append("Server offline ").append(EmojiParser.parseToUnicode(sadEmoji.get(new Random().nextInt(sadEmoji.size()))));
        }
        return sb.toString();
    }

    private String getBase() {
        StringBuilder sb = new StringBuilder();
        fileTransfer = new FileTransfer();
        fileTransfer.sftpConnection(hostName);
        try {
            fileTransfer.checkConnection();
            sb.append("db :\n")
                    .append(fileTransfer.getBase());
        } catch (JSchException e) {
            sb.append("Server offline ").append(EmojiParser.parseToUnicode(sadEmoji.get(new Random().nextInt(sadEmoji.size()))));
        }
        return sb.toString();
    }

    private String setTime() {
        StringBuilder sb = new StringBuilder();
        fileTransfer = new FileTransfer();
        fileTransfer.sftpConnection(hostName);
        try {
            fileTransfer.checkConnection();
            sb.append(fileTransfer.executeCommand("date -s \"" + bigDate + "\""));
        } catch (JSchException e) {
            sb.append("Server offline ").append(EmojiParser.parseToUnicode(sadEmoji.get(new Random().nextInt(sadEmoji.size()))));
        }
        return sb.toString();
    }

    private String getLog() {
        StringBuilder sb = new StringBuilder();
        fileTransfer = new FileTransfer();
        fileTransfer.sftpConnection(hostName);
        try {
            fileTransfer.checkConnection();
            sb.append(fileTransfer.executeCommand("tail -f -n " + strings + " /opt/glassfish4/glassfish/domains/storm/logs/server.log"));
        } catch (JSchException e) {
            sb.append("Server offline ").append(EmojiParser.parseToUnicode(sadEmoji.get(new Random().nextInt(sadEmoji.size()))));
        }
        return sb.toString();
    }

    private String getAllInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append(hostName).append("\n\n");
        fileTransfer = new FileTransfer();
        fileTransfer.sftpConnection(hostName);

        String tempWeb = "";

        /* Get version from api
        String asGenVersion = "";
        try {
            ApiConnector apiConnector = new ApiConnector(hostName);
            apiConnector.connect();
            asGenVersion = apiConnector.getVersion();
            apiConnector.disconnect();
        } catch (Exception ignored){
        }*/

        try {
            fileTransfer.checkConnection();
            tempWeb = fileTransfer.executeCommand("cd /home/storm/.alee/storm3web; ls -td -- */ | head -n 1 | cut -d'/' -f1");
            tempWeb = tempWeb.substring(0,tempWeb.indexOf("\n"));
            logger.info("trying to get web version from " + "/home/storm/.alee/storm3web/" + tempWeb + "/version.xml");
            sb.append("AS :")
                    //.append(asGenVersion).append(" ")
                    .append(fileTransfer.getInfoFormFile("/home/zForUpdateServer/asVersion.txt"))
                    .append("WEB :")
                    .append(tempWeb).append(" ")
                    .append(fileTransfer.getInfoFormFile("/home/storm/.alee/storm3web/" + tempWeb + "/version.xml"))
                    .append("db :\n")
                    .append(fileTransfer.getBase())
                    .append("GF apps: \n")
                    .append(fileTransfer.getGlassfishApps());
            sb = new StringBuilder(sb.substring(0, sb.indexOf("Command")));

            textArea.setStyle("-fx-background-color: #b4e78d");
            textArea.setPrefSize(250.0, 250.0);
        } catch (JSchException e) {
            sb.append("Server offline ").append(EmojiParser.parseToUnicode(sadEmoji.get(new Random().nextInt(sadEmoji.size()))));
            textArea.setStyle("-fx-background-color: #ee9999");
            textArea.setPrefSize(150.0, 100.0);
        }
        textArea.setText(sb.toString());
        return sb.toString();
    }

    @Override
    public void run() {
        fileTransfer = new FileTransfer();
        fileTransfer.sftpConnection(hostName);

        switch (iDoperation) {
            case 0: {
                this.result = executeGFCmd("/etc/init.d/GlassFish_storm start");
                break;
            }
            case 1: {
                this.result = executeGFCmd("/etc/init.d/GlassFish_storm stop");
                break;
            }
            case 2: {
                this.result = executeGFCmd("/etc/init.d/GlassFish_storm restart");
                break;
            }
            case 3: {
                this.result = getLog();
                break;
            }
            case 4: {
                this.result = executeGFCmd("pkill -9 java");
                break;
            }
            case 5: {
                this.result = getAllInfo();
                break;
            }
            case 6: {
                this.result = getGfApps();
                break;
            }
            case 7: {
                try {
                    fileTransfer.checkConnection();
                    onoffStatus = true;
                } catch (JSchException e) {
                    onoffStatus = false;
                }
                break;
            }
            case 8: {
                this.result = getVersions();
                break;
            }
            case 9: {
                this.result = getBase();
                break;
            }
            case 10: {
                this.result = executeGFCmd("/opt/glassfish4/glassfish/bin/asadmin --user admin -W /home/zForUpdateServer/password.txt enable " + webAppName);
                break;
            }
            case 11: {
                this.result = executeGFCmd("/opt/glassfish4/glassfish/bin/asadmin --user admin -W /home/zForUpdateServer/password.txt disable " + webAppName);
                break;
            }
            case 12: {
                this.result = executeGFCmd("/opt/glassfish4/glassfish/bin/asadmin --user admin -W /home/zForUpdateServer/password.txt disable " + webAppName);
                this.result = this.result.concat(executeGFCmd("/opt/glassfish4/glassfish/bin/asadmin --user admin -W /home/zForUpdateServer/password.txt enable " + webAppName));
                break;
            }
            case 13: {
                this.result = setTime();
                break;
            }
            default: {
                break;
            }
        }
        fileTransfer.closeConnect();
    }

    public synchronized String getResult() {
        return result;
    }

    @Override
    public String call() throws Exception {
        run();
        return result;
    }
}
