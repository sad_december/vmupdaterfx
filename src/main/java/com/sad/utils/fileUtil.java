package com.sad.utils;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;
import java.util.Optional;

public class fileUtil {
    private static final Logger logger = LogManager.getLogger(fileUtil.class);

    public fileUtil() {
    }

    //Get lastChangeDate file
    public File getMostRecentFile(Path pathToFolder){
        File file = null;
        Optional<File> mostRecentFile =
                Arrays.stream(Objects.requireNonNull(pathToFolder.toFile().listFiles()))
                        .max(Comparator.comparingLong(File::lastModified));
        if (mostRecentFile.isPresent()) {
            file = mostRecentFile.get();
        }
        assert file != null;
        logger.info("Last changed file is : " + file.getName());
        return file;
    }

    public File getMostRecentFile(Path pathToFolder, String contains){
        File fl = new File(pathToFolder.toString());
        File[] files = fl.listFiles(File::isFile);
        long lastMod = Long.MIN_VALUE;
        File choice = null;
        assert files != null;
        for (File file : files) {
            if (file.lastModified() > lastMod && file.getName().contains(contains)) {
                choice = file;
                lastMod = file.lastModified();
            }
        }
        return choice;
    }

    //CreateFolder
    public void createFolder(String path){
        try {
            Files.createDirectories(Paths.get(path));
            logger.info("Created : " + path);
        }catch (Exception e){
            logger.error(e);
        }
    }

    //Uzip to folder via zip4j in win
    public void unzip(String source, String destination){
        String password = "";
        try {
            logger.info("Start unziping : " + source);
            ZipFile zipFile = new ZipFile(source);
            if (zipFile.isEncrypted()) {
                zipFile.setPassword(password);
            }
            zipFile.extractAll(destination);
            logger.info("Unzip completed : " + source);
        } catch (ZipException e) {
            logger.error(e);
        }
    }

    //deleted wrong plugins
    public void deleteFiles(String fileDirectory, String serverPlugins) {
        File direcotry = new File(fileDirectory);
        for (File file : Objects.requireNonNull(direcotry.listFiles())){
            if (!serverPlugins.contains(file.getName())){
                file.delete();
            }
        }
        logger.info("Deleted files via template");
    }

    public void copyFolder(String from, String where) {
        try {
            FileUtils.copyDirectory(new File(from), new File(where));
            logger.info("Copy : " + from + " to " + where);
        } catch (IOException e) {
            logger.error(e);
        }
    }

    public void copyFile(String filePath, String filePathCopied) {
        try {
            FileUtils.copyFile(new File(filePath), new File(filePathCopied));
            logger.info("Copy file : " + filePath + " to " + filePathCopied);
        } catch (IOException e) {
            logger.error(e);
        }
    }

    //recursive folder deletion
    public boolean deleteDirectory(File dir) {
        if(! dir.exists() || !dir.isDirectory())    {
            return false;
        }
        String[] files = dir.list();
        for(int i = 0, len = files.length; i < len; i++)    {
            File f = new File(dir, files[i]);
            if(f.isDirectory()) {
                deleteDirectory(f);
            }else   {
                f.delete();
            }
        }
        return dir.delete();
    }

    //adding folder to zip
    public void addToZip(String sourceZipFile, String folderToAdd){
        String password = "";
        try{
            ZipFile zipFile = new ZipFile(sourceZipFile);
            if(zipFile.isEncrypted()) {
                zipFile.setPassword(password);
            }
            zipFile.addFolder(folderToAdd, new ZipParameters());

            logger.info("Zip completed : " + sourceZipFile + " <- " + folderToAdd);
        } catch (ZipException e) {
            logger.error(e);
        }
    }

    public void addFileToZip(String sourceZipFile, String fileToAdd){
        String password = "";
        try{
            ZipFile zipFile = new ZipFile(sourceZipFile);
            if (zipFile.isEncrypted()) {
                zipFile.setPassword(password);
            }
            zipFile.addFile(new File(fileToAdd), new ZipParameters());

            logger.info("Zip completed : " + sourceZipFile + " <- " + fileToAdd);
        } catch (ZipException e) {
            e.printStackTrace();
        }
    }

    public void writer(File toWrite, String whatToWrite) {
        try {
            FileUtils.writeStringToFile(toWrite, whatToWrite);
            logger.info("Writed " + whatToWrite + " into file " + toWrite.getName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
