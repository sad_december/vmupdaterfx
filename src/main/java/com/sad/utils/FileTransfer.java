package com.sad.utils;

import com.jcraft.jsch.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.*;

public class FileTransfer {

    private static final Logger logger = LogManager.getLogger(FileTransfer.class);

    private JSch jsch = new JSch();
    private Session session = null;
    private String hostnaForInfoOnly;

    public void sftpConnection(String hostName) {
        // Variable Declaration.
        String user = "root";
        hostnaForInfoOnly = hostName;
        int port = 22;
        String password = "root";

        try {
            session = jsch.getSession(user, hostName, port);

            session.setConfig("StrictHostKeyChecking", "no");
            session.setTimeout(1000);
            session.setPassword(password);

            session.connect();
            logger.info("SESSION CONNECTED");
        } catch (JSchException e) {
            logger.error(e);
        }
    }

    public void sendFileToServer(String file, String outPutDir) {
        try {
            Channel channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp sftpChannel = (ChannelSftp) channel;
            logger.info("SFTP CHANNEL CONNECTED");

            sftpChannel.put(file, outPutDir, ChannelSftp.OVERWRITE);
            logger.info("MOVED " + file + " to " + outPutDir);

            sftpChannel.exit();
        } catch (SftpException | JSchException e) {
            logger.error(e);
        }
    }

    public void closeConnect() {
        session.disconnect();
        logger.info("SFTP session closed");
    }


    public void putFile(File file, ChannelSftp sftpChannel, String outputDir) {

        FileInputStream fis = null;
        try {
            // Change to output directory.
            try {
                sftpChannel.cd(outputDir);
            } catch (SftpException e) {
                logger.error(e);
            }

            // Upload file.
            fis = new FileInputStream(file);
            sftpChannel.put(fis, file.getName());
            fis.close();
        } catch (IOException | SftpException e) {
            logger.error(e);
        }
    }

    public static File[] findFile(String dirName, final String mask) {
        File dir = new File(dirName);

        return dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String filename) {
                return filename.endsWith(mask);
            }
        });
    }

    public String executeCommand(String command) throws JSchException {

        StringBuilder outputBuffer = new StringBuilder();
        try {
            Channel channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand(command);
            InputStream commandOutput = channel.getInputStream();
            channel.connect();
            int readByte = commandOutput.read();
            while (readByte != 0xffffffff) {
                outputBuffer.append((char) readByte);
                readByte = commandOutput.read();
            }
            channel.disconnect();
            logger.info("Executed command " + command + "\nResult : " + outputBuffer.toString());
        } catch (IOException e) {
            logger.error(e);
        }
        return outputBuffer.toString();
    }

    public void checkConnection() throws JSchException {
        Channel channel = session.openChannel("exec");
        channel.disconnect();
        logger.info("Checking connection to " + this.hostnaForInfoOnly);
    }

    public String getBase() {
        StringBuilder temp = new StringBuilder("");
        try {
            Channel channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp sftpChannel = (ChannelSftp) channel;
            InputStream stream = sftpChannel.get("/home/storm/.alee/AleeArchiveServer3/conf/datasource.properties");
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(stream));
                String line;
                while ((line = br.readLine()) != null) {
                    if (line.contains("databaseAddress") && !line.contains("#"))
                        temp.append(line).append("\n");
                    if (line.contains("databaseName") && !line.contains("#"))
                        temp.append(line).append("\n");
                }
                channel.disconnect();
            } catch (Exception io) {
                logger.error("Exception occurred during reading file from SFTP server due to " + io.getMessage());
            }
        } catch (JSchException | SftpException e) {
            logger.error(e);
        }
        return temp.toString();
    }

    public String getInfoFormFile(String filePath) throws JSchException {
        StringBuilder temp = new StringBuilder("");

        Channel channel = session.openChannel("sftp");
        channel.connect();
        ChannelSftp sftpChannel = (ChannelSftp) channel;
        InputStream stream = null;
        try {
            stream = sftpChannel.get(filePath);
        } catch (SftpException e) {
            e.printStackTrace();
        }
        try {
            assert stream != null;
            BufferedReader br = new BufferedReader(new InputStreamReader(stream));
            String line;
            while ((line = br.readLine()) != null) {
                temp.append(line).append("\n");
            }
            channel.disconnect();
        } catch (Exception io) {
            logger.warn("Exception occurred during reading file from SFTP server due to " + io.toString());
            return "\n";
        }
        if (temp.toString().contains("<Version")){
            return temp.toString().substring(temp.indexOf("<Version") + "<Version".length(), temp.indexOf("/>")) + "\n";
        }
        return temp.toString();
    }

    public String getGlassfishApps() {
        StringBuilder outputBuffer = new StringBuilder();
        try {
            Channel channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand("/opt/glassfish4/glassfish/bin/asadmin --user admin --passwordfile /home/zForUpdateServer/password.txt list-applications");
            InputStream commandOutput = channel.getInputStream();
            channel.connect();
            int readByte = commandOutput.read();
            while (readByte != 0xffffffff) {
                outputBuffer.append((char) readByte);
                readByte = commandOutput.read();
            }
            channel.disconnect();
        } catch (JSchException | IOException e) {
            logger.error(e);
        }
        return outputBuffer.toString();
    }
}