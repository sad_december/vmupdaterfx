package com.sad.utils;

public class ShittySettings {

    public String defaultServer = "BookTabs.jar\n" +
            "CatalogueExport.jar\n" +
            "Comments.jar\n" +
            "Duplicates.jar\n" +
            "EbsConfiguration.jar\n" +
            "expiring-report-server.jar\n" +
            "FastInput.jar\n" +
            "Favorites.jar\n" +
            "fill-report-server.jar\n" +
            "fund-report-server.jar\n" +
            "PublicationsOffline.jar\n" +
            "registration-server.jar\n" +
            "reports-server.jar\n" +
            "SavedSearch.jar\n" +
            "usage-report-server.jar\n" +
            "UserData.jar\n" +
            "ViewerCopies.jar\n" +
            "ViewerLink.jar\n" +
            "workflow-v2.jar\n" +
            "xml-export-server.jar\n";

    public String defaultClient = "BookTabs.jar\n" +
            "CatalogueExport.jar\n" +
            "Duplicates.jar\n" +
            "FastInput.jar\n" +
            "MassInheritance.jar\n" +
            "MGOK.jar\n" +
            "PublicationsOffline.jar\n" +
            "registration-client.jar\n" +
            "SavedSearch.jar\n" +
            "av-excel-import-client.jar\n" +
            "workflow-v2.jar\n";

    public String ebsServer = "BookTabs.jar\n" +
            "Comments.jar\n" +
            "DIPImport.jar\n" +
            "EbsConfiguration.jar\n" +
            "FastInput.jar\n" +
            "Favorites.jar\n" +
            "fill-report-server.jar\n" +
            "FullPreviewFiles.jar\n" +
            "fund-report-server.jar\n" +
            "HelpPlugin.jar\n" +
            "PublicFileAccess.jar\n" +
            "registration-server.jar\n" +
            "reports-server.jar\n" +
            "SavedSearch.jar\n" +
            "usage-report-server.jar\n" +
            "UserData.jar\n" +
            "ViewerCopies.jar\n";

    public String ebsClient = "BookTabs.jar\n" +
            "DIPImport.jar\n" +
            "PublicationsOffline.jar\n" +
            "registration-client.jar\n" +
            "SavedSearch.jar\n";

    public String heliServer = "BookTabs.jar\n" +
            "CatalogueExport.jar\n" +
            "Comments.jar\n" +
            "ConsideredCopies.jar\n" +
            "custom-considered-copies-report-server.jar\n" +
            "custom-nd-change-report-server.jar\n" +
            "custom-subscriptions-report-server.jar\n" +
            "Duplicates.jar\n" +
            "EbsConfiguration.jar\n" +
            "expiring-report-server.jar\n" +
            "FastInput.jar\n" +
            "Favorites.jar\n" +
            "file-binder-server.jar\n" +
            "fill-report-server.jar\n" +
            "fund-report-server.jar\n" +
            "linked-report-server.jar\n" +
            "nd-changes-server.jar\n" +
            "ND-subscribe.jar\n" +
            "NormativeControl.jar\n" +
            "PublicationsOffline.jar\n" +
            "reports-server.jar\n" +
            "Reviews.jar\n" +
            "SavedSearch.jar\n" +
            "stubfile-server.jar\n" +
            "usage-report-server.jar\n" +
            "UserData.jar\n" +
            "ViewerCopies.jar\n" +
            "ViewerLink.jar\n" +
            "Voting.jar\n" +
            "workflow-v1.jar\n" +
            "xml-export-server.jar\n";

    public String heliClient = "BookTabs.jar\n" +
            "file-binder-client.jar\n" +
            "CatalogueExport.jar\n" +
            "ConsideredCopies.jar\n" +
            "Duplicates.jar\n" +
            "FastInput.jar\n" +
            "MassInheritance.jar\n" +
            "ND-subscribe.jar\n" +
            "NormativeControl.jar\n" +
            "PublicationsOffline.jar\n" +
            "SavedSearch.jar\n" +
            "stubfile-client.jar\n" +
            "workflow-v1.jar\n";

    public String mchsServer = "BookTabs.jar\n" +
            "Comments.jar\n" +
            "EbsConfiguration.jar\n" +
            "Favorites.jar\n" +
            "fill-report-server.jar\n" +
            "FullPreviewFiles.jar\n" +
            "fund-report-server.jar\n" +
            "HelpPlugin.jar\n" +
            "PublicFileAccess.jar\n" +
            "registration-server.jar\n" +
            "reports-server.jar\n" +
            "SavedSearch.jar\n" +
            "synchronization-server.jar\n" +
            "usage-report-server.jar\n" +
            "UserData.jar\n" +
            "ViewerCopies.jar\n";

    public String mchsClient = "registration-client.jar";

    public String omkServer = "BookTabs.jar\n" +
            "CatalogueExport.jar\n" +
            "Comments.jar\n" +
            "ConsideredCopies.jar\n" +
            "custom-considered-copies-report-server.jar\n" +
            "custom-nd-change-report-server.jar\n" +
            "custom-subscriptions-report-server.jar\n" +
            "EbsConfiguration.jar\n" +
            "expiring-report-server.jar\n" +
            "Favorites.jar\n" +
            "fund-report-server.jar\n" +
            "nd-changes-server.jar\n" +
            "ND-subscribe.jar\n" +
            "NormativeControl.jar\n" +
            "omk-extension-server.jar\n" +
            "omk-wss-server.jar\n" +
            "PublicationsOffline.jar\n" +
            "reports-server.jar\n" +
            "SavedSearch.jar\n" +
            "usage-report-server.jar\n" +
            "UserData.jar\n" +
            "workflow-v2.jar\n";

    public String omkClient = "BookTabs.jar\n" +
            "CatalogueExport.jar\n" +
            "ConsideredCopies.jar\n" +
            "ND-subscribe.jar\n" +
            "NormativeControl.jar\n" +
            "PublicationsOffline.jar\n" +
            "SavedSearch.jar\n" +
            "workflow-v2.jar\n";

    public String cqmsServer = "BookTabs.jar\n" +
            "CatalogueExport.jar\n" +
            "Comments.jar\n" +
            "ConsideredCopies.jar\n" +
            "cqms-audits-server.jar\n" +
            "cqms-fixation-base-crm-server.jar\n" +
            "cqms-fixation-base-server.jar\n" +
            "cqms-monitoring-server.jar\n" +
            "cqms-risks-server.jar\n" +
            "custom-considered-copies-report-server.jar\n" +
            "custom-nd-change-report-server.jar\n" +
            "custom-subscriptions-report-server.jar\n" +
            "Duplicates.jar\n" +
            "EbsConfiguration.jar\n" +
            "expiring-report-server.jar\n" +
            "FastInput.jar\n" +
            "Favorites.jar\n" +
            "fill-report-server.jar\n" +
            "fund-report-server.jar\n" +
            "nd-changes-server.jar\n" +
            "ND-subscribe.jar\n" +
            "NormativeControl.jar\n" +
            "PublicationsOffline.jar\n" +
            "registration-server.jar\n" +
            "reports-server.jar\n" +
            "SavedSearch.jar\n" +
            "usage-report-server.jar\n" +
            "UserData.jar\n" +
            "ViewerCopies.jar\n" +
            "ViewerLink.jar\n" +
            "workflow-v2.jar\n" +
            "xml-export-server.jar\n";

    public String cqmsClient = "BookTabs.jar\n" +
            "CatalogueExport.jar\n" +
            "ConsideredCopies.jar\n" +
            "Duplicates.jar\n" +
            "FastInput.jar\n" +
            "MassInheritance.jar\n" +
            "ND-subscribe.jar\n" +
            "NormativeControl.jar\n" +
            "PublicationsOffline.jar\n" +
            "registration-client.jar\n" +
            "SavedSearch.jar\n" +
            "workflow-v2.jar\n";

    public String kbntiServer = "xml-export-server.jar\n" +
            "workflow-v2.jar\n" +
            "Voting.jar\n" +
            "ViewerLink.jar\n" +
            "ViewerCopies.jar\n" +
            "UserData.jar\n" +
            "usage-report-server.jar\n" +
            "synchronization-server.jar\n" +
            "stubfile-server.jar\n" +
            "SavedSearch.jar\n" +
            "Reviews.jar\n" +
            "reports-server.jar\n" +
            "registration-server.jar\n" +
            "PublicFileAccess.jar\n" +
            "PublicationsOffline.jar\n" +
            "NormdocsConverters.jar\n" +
            "NormativeControl.jar\n" +
            "ND-subscribe.jar\n" +
            "nd-changes-server.jar\n" +
            "linked-report-server.jar\n" +
            "kbntind-extension-server.jar\n" +
            "HelpPlugin.jar\n" +
            "fund-report-server.jar\n" +
            "FullPreviewFiles.jar\n" +
            "fill-report-server.jar\n" +
            "file-binder-server.jar\n" +
            "Favorites.jar\n" +
            "FastInput.jar\n" +
            "expiring-report-server.jar\n" +
            "EbsConfiguration.jar\n" +
            "Duplicates.jar\n" +
            "document-replacer-server.jar\n" +
            "DocumentReplacer.jar\n" +
            "DIPImport.jar\n" +
            "custom-subscriptions-report-server.jar\n" +
            "custom-nd-change-report-server.jar\n" +
            "custom-considered-copies-report-server.jar\n" +
            "cqms-risks-server.jar\n" +
            "cqms-monitoring-server.jar\n" +
            "cqms-fixation-base-server.jar\n" +
            "cqms-fixation-base-crm-server.jar\n" +
            "cqms-audits-server.jar\n" +
            "considered-copies-workflow-server.jar\n" +
            "ConsideredCopies.jar\n" +
            "Comments.jar\n" +
            "CatalogueExport.jar\n" +
            "BookTabs.jar\n" +
            "Actualization.jar\n";

    public String kbntiClient = "workflow-v2.jar\n" +
            "stubfile-client.jar\n" +
            "SavedSearch.jar\n" +
            "registration-client.jar\n" +
            "PublicationsOffline.jar\n" +
            "NormdocsConverters.jar\n" +
            "NormativeControl.jar\n" +
            "ND-subscribe.jar\n" +
            "MGOK.jar\n" +
            "MassInheritance.jar\n" +
            "file-binder-client.jar\n" +
            "FastInput.jar\n" +
            "Duplicates.jar\n" +
            "DIPImport.jar\n" +
            "considered-copies-workflow-client.jar\n" +
            "ConsideredCopies.jar\n" +
            "CatalogueExport.jar\n" +
            "av-excel-import-client.jar\n" +
            "Actualization.jar\n";

    public String cleanServer = "";
    public String cleanClient = "";
}