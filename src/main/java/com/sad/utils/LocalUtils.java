package com.sad.utils;

import java.util.ArrayList;

public class LocalUtils {

    public ArrayList<String> getHosts() {
        return hosts;
    }

    public ArrayList<String> getSadEmoji() {
        return sadEmoji;
    }

    private ArrayList<String> hosts;

    private ArrayList<String> sadEmoji;

    public LocalUtils() {
        fillHostsList();
        fillSadEmojiList();
    }

    private void fillHostsList() {
        hosts = new ArrayList<>();
        hosts.add("testvm");
        hosts.add("testvm3");
        hosts.add("testvm6");
        hosts.add("testvm7");
        hosts.add("testvm10");
        hosts.add("testvm11");
        hosts.add("testvm12");
        hosts.add("testvm13");
        hosts.add("testvm14");
    }

    private void fillSadEmojiList() {
        sadEmoji = new ArrayList<>();
        sadEmoji.add(":pensive:");
        sadEmoji.add(":unamused:");
        sadEmoji.add(":persevere:");
        sadEmoji.add(":tired_face:");
        sadEmoji.add(":worried:");
        sadEmoji.add(":anguished:");
        sadEmoji.add(":confused:");
        sadEmoji.add(":disappointed:");
        sadEmoji.add(":angry:");
        sadEmoji.add(":sleeping:");
    }

    public String getTextInfoAboutHost(String host) {
        switch (host){
            case "testvm":{
                return "Historical info : testvm - Default";
            }
            case "testvm3":{
                return "Historical info : testvm3 - EBS";
            }
            case "testvm6":{
                return "Historical info : testvm6 - KBNTI2 OMK";
            }
            case "testvm7":{
                return "Historical info : testvm7 - MCHS Central\nDont touch or I find you";
            }
            case "testvm10":{
                return "Historical info : testvm10 - Helicopters";
            }
            case "testvm11":{
                return "Historical info : testvm11 - MCHS Local";
            }
            case "testvm12":{
                return "Historical info : testvm12 - KBNTI2 for OMK/TMK + converters";
            }
            case "testvm13":{
                return "Historical info : testvm13 - KBNTI2 for OMK/TMK";
            }
            case "testvm14":{
                return "Historical info : testvm14 - KBNTI2 + CQSMS(КСМК) for TMK";
            }
            default:
                return "no info about host";
        }
    }
}
