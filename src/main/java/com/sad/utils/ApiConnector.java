package com.sad.utils;

public class ApiConnector {
    private String host = "localhost";
    private int port = 8080;
    private String user = "";
    private String password = "";
    //Archive3ServerConnector storm;

    public ApiConnector(String hostName){
        this.host = hostName;
    }

    public ApiConnector(){

    }

    public ApiConnector(String host, int port, String user, String password) {
        this.host = host;
        this.port = port;
        this.user = user;
        this.password = password;
    }

    public void connect(){
        //storm = new Archive3ServerConnector(host, port, false);
    }

    public String getVersion(){
        //return storm.getAuthService().getServerVersion().toString();
        return "";
    }

    public void disconnect(){
        //storm.logoff();
    }
}