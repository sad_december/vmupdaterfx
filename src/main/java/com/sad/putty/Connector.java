package com.sad.putty;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class Connector extends Thread {
    JSch jsch = new JSch();
    String user = "root";
    String host = "";
    Integer port = 22;
    String password = "root";
    boolean online = false;

    Connector(String host) {
        this.host = host;
    }

    @Override
    public synchronized void start()  {
        try {
            check();
            online = true;
        } catch (JSchException ignored) {
        }
    }

    private synchronized void check() throws JSchException{
        Session session = null;
        session = jsch.getSession(user, host, port);
        session.setConfig("StrictHostKeyChecking", "no");
        session.setTimeout(1000);
        session.setPassword(password);
        session.connect();
        Channel channel = session.openChannel("exec");
        channel.disconnect();
        session.disconnect();
    }
}