package com.sad.putty;

import java.io.IOException;
import java.util.ArrayList;

public class PuttyWorker extends Thread {
    private String puttyPath;
    private ArrayList<String> enabledHostst;

    public PuttyWorker(String puttyPath, ArrayList<String> enabledHostst){
        this.puttyPath = puttyPath;
        this.enabledHostst = enabledHostst;
    }

    @Override
    public void run() {
        assert enabledHostst != null;
        for (String host : enabledHostst){
            try {
                Process puttyCmd = Runtime.getRuntime().exec(puttyPath + " -ssh root@" + host + " -pw root");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}