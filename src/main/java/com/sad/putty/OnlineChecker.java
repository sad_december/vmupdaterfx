package com.sad.putty;

import java.util.ArrayList;

public class OnlineChecker extends Thread {
    ArrayList<String> hosts = new ArrayList<>();
    public ArrayList<String> normalHosts = new ArrayList<>();

    @Override
    public void run() {
        filhosts();
        checkOnline();
    }

    private void checkOnline() {
        for (String host : hosts){
            Connector connector = new Connector(host);
            connector.start();
            synchronized (connector) {
                if (connector.online){
                    normalHosts.add(host);
                }
            }
        }
    }

    private void filhosts() {
    hosts.add("testvm");
        for (int i = 0; i < 16; i++){
            hosts.add("testvm" + i);
        }
    }
}
